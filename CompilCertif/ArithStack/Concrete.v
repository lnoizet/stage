Require Import String List.
Require Import Semantics.
Require Import ZArith.
Import ListNotations.
Open Scope string_scope.

Section Concrete.

  Variable l: language.
  Variable base_interp: l.(l_base_sort) -> Type.
  Variable flow_interp: l.(l_flow_sort) -> Type.
  Notation "'typed_skel_var'" := (typed_skel_var l.(l_base_sort) l.(l_flow_sort) l.(l_prgm_sort)).
  Notation "'sort'" := (sort l.(l_base_sort) l.(l_flow_sort) l.(l_prgm_sort)).
  Notation "'skel_var'" := (skel_var l.(l_base_sort) l.(l_flow_sort) l.(l_prgm_sort)).

  Inductive cterm :=
  | cterm_base : forall (b:l_base_sort l), base_interp b -> cterm
  | cterm_constructor : l.(l_cons) -> list cterm -> cterm.

  Inductive value :=
  | val_flow : forall f, flow_interp f -> value
  | val_cterm : cterm -> value.

  Definition concrete_quadruple := (l.(l_hook) * list value * cterm * list value)%type.

  Definition env := list (typed_skel_var * value).

  Notation "'void_env'" := [] (only parsing).

  Fixpoint find (e: env) (s: string) : option value :=
    match e with
    | [] => None
    | (existT _ _ (skel_var_intro _ _ _ t s'),v) :: l => if string_dec s s' then Some v else find l s
    end.

  Definition option_bind {A B} (x : option A) (f : A -> option B) := 
    match x with
    | None => None
    | Some x' => f x'
    end.

  Notation "x <- y ; z" := (option_bind y (fun x => z))
    (right associativity, at level 55, y at level 53).

  Definition Cstate := (env * (concrete_quadruple -> Prop))%type.
  Definition Cresult := env.

  (* Pointless
  Fixpoint add_asn_unique (e:env) (s:skel_var) (v:value) : env :=
    match e with
    | [] => []
    | (s1,v1) :: q => if string_dec s1 s then (s,v) :: q else
        (s1,v1) :: (add_asn_unique q s v)
    end.
        *)

  Fixpoint add_asn (f : env) (l:list typed_skel_var) (lv:list value) : env :=
    match l, lv with
    | s::l', c::lv' => add_asn ((s,c)::f) l' lv'
    | [], [] => f
    | _, _ => void_env
    end.

  Fixpoint add_asn_cterm (f : env) (l:list typed_skel_var) (lv:list cterm) : env :=
    match l, lv with
    | s::l', c::lv' => add_asn_cterm ((s,val_cterm c)::f) l' lv'
    | [], [] => f
    | _, _ => void_env
    end.

  Variable interpFC  : l.(l_filter) -> list value -> list value -> Prop.

  Fixpoint unfold_list_option {T} (l : list (option T)) : option (list T) :=
    match l with
    | [] => Some []
    | None :: q => None
    | (Some x) :: q => match unfold_list_option q with
    | Some l => Some (x::l)
    | None => None
    end
    end.

  Notation "'interpFC_opt'" :=
    (fun f l1 l2 => match unfold_list_option l1 with
    | None => False
    | Some l1 => interpFC f l1 l2
    end) (only parsing).

  Definition toCterm (v:value) : option cterm :=
    match v with
    | val_flow _ _ => None
    | val_cterm c => Some c
    end.

  Fixpoint eval_list_term {sl} env (l:list_term _ _ _ _ _ sl): option (list cterm) :=
    match l with
    | nil_term _ _ _ _ _ => Some []
    | cons_term _ _ _ _ _ t rest =>
        match eval_term env t, eval_list_term env rest with
        | Some x,Some l => Some (x :: l)
        | _,_ => None
        end
    end

  with eval_term {s} env (t: term _ (l_base_sort l) (l_flow_sort l) (l_prgm_sort l) (l_csort l) s) : option cterm :=
    match t with
    | term_sv _ _ _ _ _ _ (skel_var_intro _ _ _ _ s) => val <- find env s; toCterm val
    | term_constructor _ _ _ _ _ c tl =>
      l <- eval_list_term env tl;
      Some (cterm_constructor c l)
    end.

  Inductive interpC : (skeleton _ _ _ _ _ _ _) -> Cstate -> Cresult -> Prop :=
  | i_Cons : forall B S s1 s2 T r,
      interpBC B (s1,T) s2 ->
      interpC S (s2,T) r ->
      interpC (B::S) (s1,T) r
  | i_Void : forall s t, interpC [] (s,t) s

   with

  interpBC : (bone _ _ _ _ _ _ _) -> Cstate -> Cresult -> Prop :=
  | i_F : forall f (f1 : env) tl lv (l1 : list typed_skel_var) l2,
      interpFC_opt f (List.map (find f1) (map (skel_name _ _ _) l1)) lv ->
      interpBC (F _ _ _ _ _ _ _ f l1 l2) (f1,tl) (add_asn f1 (l2) lv)
  | i_H : forall (h: (l_hook l)) (e : env) (T:concrete_quadruple -> Prop)
      (xf1 xf2 : list typed_skel_var) (t:typed_term _ _ _ _ _) (v : list value),
      match unfold_list_option (map (find e) (map (skel_name _ _ _) xf1)), eval_term e (projT2 t) with
      | Some xf1', Some t' => T (h, xf1', t', v)
      | _, _ => False
      end -> interpBC (H _ _ _ _ _ _ _ h xf1 t xf2) (e,T) (add_asn e xf2 v: env)
  | i_B : forall Ss Si V e T vals e',
      List.In Si Ss -> interpC Si (e,T) e' ->
      map (find e') (map (skel_name _ _ _) V) = map Some vals ->
      interpBC (B _ _ _ _ _ _ _ Ss V) (e,T) (add_asn e V vals).

  Inductive immediate_consequence : (concrete_quadruple -> Prop) -> concrete_quadruple -> Prop :=
  | H_intro :
      forall (h: l_hook l), forall (c: l_cons l) (s xt xo: list typed_skel_var),
      forall (S: skeleton _ _ _ _ _ _ _),
      forall lt s1 s2 T sigma,
      In (h,s,c,xt,S,xo) (l_semantics l) ->
      interpC S (add_asn (add_asn_cterm void_env xt lt) s s1, T) sigma ->
      unfold_list_option (map (find sigma) (map (skel_name _ _ _) xo)) = Some s2 ->
      immediate_consequence T (h, s1, cterm_constructor c lt, s2).

  Fixpoint consequence n :=
    match n with
    | 0 => fun _ => False
    | S m => immediate_consequence (consequence m)
            end.

  Definition concrete_semantics (t : concrete_quadruple) : Prop :=
    exists n, consequence n t.

End Concrete.

