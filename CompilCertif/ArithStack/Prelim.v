Require Arithmetic Stack.
Require Import List Ascii String.
Require Import Semantics Concrete.
Import ListNotations.
Open Scope string_scope.

Module A := Arithmetic.
Definition ar := Arithmetic.lang.
Module S := Stack.
Definition st := Stack.lang.


(* Fonctions utiles *)
Definition option_bind {A B} (x : option A) (f : A -> option B) : option B :=
  match x with
  | None => None
  | Some y => f y
  end.

Definition ap {A B} (e : A = B) (x : A) : B :=
  match e with
  | eq_refl => x
  end.

  Notation "'interpFC_opt'" :=
    (fun l iFC f l1 l2 => match unfold_list_option l1 with
    | None => False
    | Some l1 => iFC f l1 l2
    end) (only parsing).

Definition get_sort l bi fi (c:value l bi fi) : sort _ _ _:=
  match c with
  | val_cterm _ _ _ (cterm_base _ _ b _) => Term _ _ _ (Base _ _ b)
  | val_cterm _ _ _ (cterm_constructor _ _ c _) => Term _ _ _ (Prgm _ _ (snd (l_csort l c)))
  | val_flow _ _ _ f _ => Flow _ _ _ f
  end.

Definition FilterSort l bi fi iFC :=
  forall l1 l2 (f: l_filter l), iFC f l1 l2 ->
  map (get_sort l bi fi) l1 = fst (l_fsort l f) /\
  map (get_sort l bi fi) l2 = snd (l_fsort l f).


(* Base tactics *)
Ltac inj_ext :=
  match goal with
  | [ H: Some _ = Some _ |- _ ] => injection H as ?; inj_ext
  | [ H: (_, _) = (_, _) |- _] => injection H as ?; inj_ext
  | [ H: _::_ = _::_ |- _] => injection H as ?; inj_ext
  | [ H: S _ = S _ |- _] => injection H as ?; inj_ext
  | [ H: False |- _] => destruct H
  | _ => idtac
  end.

Ltac subst_ext :=
  inj_ext; subst;
  match goal with
  | [ H: _ |- _] => simpl in H; cbn in H; progress inj_ext; subst_ext
  | _ => discriminate
  | _ => subst
  end.


(* Axiomes *)
Axiom bia: l_base_sort ar -> Type.
Axiom fia: l_flow_sort ar -> Type.
Axiom bis: l_base_sort st -> Type.
Axiom fis: l_flow_sort st -> Type.
Axiom AinterpFC : (A.filter -> list (value ar bia fia) -> list (value ar bia fia) -> Prop).
Axiom SinterpFC : (S.filter -> list (value st bis fis) -> list (value st bis fis) -> Prop).

Axiom FilterArithmeticSort:
  FilterSort ar bia fia AinterpFC.
Lemma AddArithmeticSort:
  forall l1 l2,
  AinterpFC A.f_add l1 l2 -> exists a b c,
  l1 = [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b] /\
  l2 = [val_flow ar bia fia A.s_value c].
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C;  congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma SubArithmeticSort:
  forall l1 l2,
  AinterpFC Arithmetic.f_sub l1 l2 -> exists a b c,
  l1 = [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b] /\
  l2 = [val_flow ar bia fia A.s_value c].
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma MulArithmeticSort:
  forall l1 l2,
  AinterpFC Arithmetic.f_mul l1 l2 -> exists a b c,
  l1 = [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b] /\
  l2 = [val_flow ar bia fia A.s_value c].
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma DivArithmeticSort:
  forall l1 l2,
  AinterpFC Arithmetic.f_div l1 l2 -> exists a b c,
  l1 = [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b] /\
  l2 = [val_flow ar bia fia A.s_value c].
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma LitToValArithmeticSort:
  forall l1 l2,
  AinterpFC Arithmetic.f_litToVal l1 l2 -> exists a b,
  l1 = [val_cterm ar bia fia (cterm_base ar bia A.s_literal a)] /\
  l2 = [val_flow ar bia fia A.s_value b].
Proof.
  intros. apply FilterArithmeticSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|? ?]]; simpl in *; try congruence. inversion H1 as [A]; subst.
  destruct l2 as [|j [|? ?]]; simpl in *; try congruence. inversion H2 as [B]; subst.
  destruct i as [? ?|[[] v |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  clear A B. subst_ext. exists v, v'. tauto.
Qed.

Axiom FilterStackSort:
  FilterSort st bis fis SinterpFC.

Lemma AddStackSort:
  forall l1 l2,
  SinterpFC Stack.f_add l1 l2 -> exists a b c,
  l1 = [val_flow st bis fis S.s_value a; 
  val_flow st bis fis Stack.s_value b] /\
  l2 = [val_flow st bis fis Stack.s_value c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma SubStackSort:
  forall l1 l2,
  SinterpFC Stack.f_sub l1 l2 -> exists a b c,
  l1 = [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_value b] /\
  l2 = [val_flow st bis fis S.s_value c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma MulStackSort:
  forall l1 l2,
  SinterpFC Stack.f_mul l1 l2 -> exists a b c,
  l1 = [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_value b] /\
  l2 = [val_flow st bis fis S.s_value c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma DivStackSort:
  forall l1 l2,
  SinterpFC Stack.f_div l1 l2 -> exists a b c,
  l1 = [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_value b] /\
  l2 = [val_flow st bis fis S.s_value c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma LitToValStackSort:
  forall l1 l2,
  SinterpFC Stack.f_litToVal l1 l2 -> exists a b,
  l1 = [val_cterm st bis fis (cterm_base st bis S.s_literal a)] /\
  l2 = [val_flow st bis fis S.s_value b].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|? ?]]; simpl in *; try congruence. inversion H1 as [A]; subst.
  destruct l2 as [|j [|? ?]]; simpl in *; try congruence. inversion H2 as [B]; subst.
  destruct i as [? ?|[[] v|[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  clear A B. subst_ext. exists v, v'. tauto.
Qed.
Lemma PushStackSort:
  forall l1 l2,
  SinterpFC Stack.f_push l1 l2 -> exists a b c,
  l1 = [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_stack b] /\
  l2 = [val_flow st bis fis S.s_stack c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|j [|? ?]]]; simpl in *; try congruence. inversion H1 as [[A B]]; subst.
  destruct l2 as [|k [|? ?]]; simpl in *; try congruence. inversion H2 as [C]; subst.
  destruct i as [? v|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.
Lemma PopStackSort:
  forall l1 l2,
  SinterpFC Stack.f_pop l1 l2 -> exists a b c,
  l1 = [val_flow st bis fis S.s_stack a] /\
  l2 = [val_flow st bis fis S.s_value b; 
  val_flow st bis fis S.s_stack c].
Proof.
  intros. apply FilterStackSort in H; destruct H as [H1 H2].
  destruct l1 as [|i [|? ?]]; simpl in *; try congruence. inversion H1 as [[A]]; subst.
  destruct l2 as [|j [|k [|? ?]]]; simpl in *; try congruence. inversion H2 as [[B C]]; subst.
  destruct i as [? v|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in A; congruence).
  destruct j as [? v'|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in B; congruence).
  destruct k as [? w|[[] |[| | | | | |] ?]]; simpl in *; try congruence;
  try (simpl in *; destruct list_eq_dec in C; congruence).
  clear A B C. subst_ext. exists v, v', w. tauto.
Qed.

Axiom StackPopFun :
  forall (a : fis Stack.s_stack),
  forall (b b' : fis Stack.s_value),
  forall (c c' : fis Stack.s_stack),
  SinterpFC Stack.f_pop
  [val_flow st bis fis S.s_stack a]
  [val_flow st bis fis S.s_value b;
  val_flow st bis fis S.s_stack c] ->
  SinterpFC Stack.f_pop
  [val_flow st bis fis S.s_stack a]
  [val_flow st bis fis S.s_value b';
  val_flow st bis fis S.s_stack c'] ->
  b = b' /\ c = c'.
Axiom StackPushFun :
  forall (a : fis Stack.s_value),
  forall (b : fis Stack.s_stack),
  forall (c c' : fis Stack.s_stack),
  SinterpFC Stack.f_push
  [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_stack b]
  [val_flow st bis fis S.s_stack c] ->
  SinterpFC Stack.f_push
  [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_stack b]
  [val_flow st bis fis S.s_stack c'] ->
  c = c'.
Axiom StackPushDef :
  forall (a : fis Stack.s_value),
  forall (b : fis Stack.s_stack),
  exists (c : fis Stack.s_stack),
  SinterpFC Stack.f_push
  [val_flow st bis fis S.s_value a; 
  val_flow st bis fis S.s_stack b]
  [val_flow st bis fis S.s_stack c].
Axiom StackPushPop :
  forall l1 l2 l3,
  SinterpFC Stack.f_push l1 l2 ->
  SinterpFC Stack.f_pop l2 l3 -> l1 = l3.
Axiom StackPushPopDef :
  forall l1 l2,
  SinterpFC Stack.f_push l1 l2 ->
  SinterpFC Stack.f_pop l2 l1.

Axiom StackLitToValFun :
  forall n,
  forall (v v' : fis Stack.s_value),
  SinterpFC Stack.f_litToVal
  [val_cterm st bis fis (cterm_base st bis S.s_literal n)]
  [val_flow st bis fis S.s_value v] ->
  SinterpFC Stack.f_litToVal
  [val_cterm st bis fis (cterm_base st bis S.s_literal n)]
  [val_flow st bis fis S.s_value v'] ->
  v = v'.
Axiom StackLitToValDef :
  forall n,
  exists (v : fis Stack.s_value),
  SinterpFC Stack.f_litToVal
  [val_cterm st bis fis (cterm_base st bis S.s_literal n)]
  [val_flow st bis fis S.s_value v].
Axiom ArithmeticLitToValDef :
  forall n,
  exists (v : fia Arithmetic.s_value),
  AinterpFC Arithmetic.f_litToVal
  [val_cterm ar bia fia (cterm_base ar bia A.s_literal n)]
  [val_flow ar bia fia A.s_value v].


Definition isInj {A B} (f : A -> B) :=
  forall x y, f x = f y -> x = y.

Definition isSurj {A B} (f : A -> B) :=
  forall y, exists x, y = f x.

Definition isomorphism {A B : Type} (f : A -> B) : Prop :=
  isInj f /\ isSurj f.

Axiom castLiteral: bia A.s_literal -> bis S.s_literal.

Axiom castValue : fia Arithmetic.s_value
  -> fis Stack.s_value.

Axiom sameValue : isomorphism castValue.

Axiom sameLitToVal :
  forall n b, 
  AinterpFC Arithmetic.f_litToVal 
  [val_cterm ar bia fia (cterm_base ar bia A.s_literal n)]
  [val_flow ar bia fia A.s_value b] <->
  SinterpFC Stack.f_litToVal
  [val_cterm st bis fis (cterm_base st bis S.s_literal (castLiteral n))]
  [val_flow st bis fis S.s_value (castValue b)].

Axiom sameAdd :
  forall a b c, 
  AinterpFC Arithmetic.f_add 
  [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b]
  [val_flow ar bia fia A.s_value c] <->
  SinterpFC Stack.f_add
  [val_flow st bis fis S.s_value (castValue a); 
  val_flow st bis fis S.s_value (castValue b)]
  [val_flow st bis fis S.s_value (castValue c)].
Axiom sameSub :
  forall a b c, 
  AinterpFC Arithmetic.f_sub 
  [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b]
  [val_flow ar bia fia A.s_value c] <->
  SinterpFC Stack.f_sub
  [val_flow st bis fis S.s_value (castValue a); 
  val_flow st bis fis S.s_value (castValue b)]
  [val_flow st bis fis S.s_value (castValue c)].
Axiom sameMul :
  forall a b c, 
  AinterpFC Arithmetic.f_mul 
  [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b]
  [val_flow ar bia fia A.s_value c] <->
  SinterpFC Stack.f_mul
  [val_flow st bis fis S.s_value (castValue a); 
  val_flow st bis fis S.s_value (castValue b)]
  [val_flow st bis fis S.s_value (castValue c)].
Axiom sameDiv :
  forall a b c, 
  AinterpFC Arithmetic.f_div 
  [val_flow ar bia fia A.s_value a; 
  val_flow ar bia fia A.s_value b]
  [val_flow ar bia fia A.s_value c] <->
  SinterpFC Stack.f_div
  [val_flow st bis fis S.s_value (castValue a); 
  val_flow st bis fis S.s_value (castValue b)]
  [val_flow st bis fis S.s_value (castValue c)].

Section CtermInd.
  Variable l: language.
  Variable bi: l_base_sort l -> Type.
  Variable P: cterm l bi -> Prop.
  Variable base: forall s n, P (cterm_base l bi s n).
  Variable cons: forall lt c, Forall P lt -> P (cterm_constructor l bi c lt).

  Fixpoint cterm_ind c: P c :=
    match c with
    | cterm_base _ _ s n => base s n
    | cterm_constructor _ _ c lt =>
        cons lt c ((fix list_cterm_ind lt: (Forall P lt) :=
          match lt with
          | [] => Forall_nil P
          | a :: rest => Forall_cons a (cterm_ind a) (list_cterm_ind rest)
          end) lt)
    end.
End CtermInd.

Section Recursivity.
  Variable c f h bs fs ps: Type.
  Variable cs: c -> list(term_sort bs ps) * ps.
  Variable (P:bone c f h bs fs ps cs -> Prop) (Q:skeleton c f h bs fs ps cs -> Prop).
  Variable (hooks:forall h i t o, P (H _ _ _ _ _ _ _ h i t o)).
  Variable (filters: forall f i o, P (F _ _ _ _ _ _ _ f i o)).
  Variable (branchs: forall Ss V, (Forall Q Ss) -> P (B _ _ _ _ _ _ _ Ss V)).
  Variable (skel: forall S, (Forall P S) -> Q S).

  Fixpoint bone_rec b {struct b} : P b :=
    match b with
    | H _ _ _ _ _ _ _ h i t o => hooks h i t o
    | F _ _ _ _ _ _ _ f i o => filters f i o
    | B _ _ _ _ _ _ _ Ss V => 
        branchs Ss V ((fix l_ind1 (l : list (skeleton c f h bs fs ps cs)) :
        (Forall Q l) :=
        match l with
        | [] => Forall_nil Q
        | sk1 :: skr => Forall_cons sk1
            (skel sk1
            ((fix l_ind2 (l: skeleton c f h bs fs ps cs) : (Forall P l) :=
            match l with
            | [] => Forall_nil P
            | b1 :: br => Forall_cons b1 (bone_rec b1) (l_ind2 br)
            end) sk1))
         (l_ind1 skr)
        end) Ss)
    end.

  Definition skel_rec s : Q s :=
  skel s (proj2 (Forall_forall P s) (fun x _ => bone_rec x)).

  Definition skel_bone_rec : (forall b, P b) /\ (forall s, Q s) :=
    conj bone_rec skel_rec.
End Recursivity.



Definition def_interpBC_inc l bi fi iFC b:=
  forall s1 (T U:concrete_quadruple l bi fi -> Prop) s2,
  (forall x, T x -> U x) ->
  interpBC l bi fi iFC b (s1, T) s2 -> interpBC l bi fi iFC b (s1, U) s2.
Definition def_interpC_inc l bi fi iFC s:=
    forall s1 (T U: concrete_quadruple l bi fi -> Prop) r1,
    (forall x, T x -> U x) ->
    interpC l bi fi iFC s (s1,T) r1 -> interpC l bi fi iFC s (s1,U) r1.


Lemma interp_inc l bi fi iFC:
  (forall b, def_interpBC_inc l bi fi iFC b) /\
  forall s, def_interpC_inc l bi fi iFC s.
Proof.
  apply skel_bone_rec.
  - intros h i t o s1 T U s2 HTU Hint.
    inversion Hint; subst; simpl in *; econstructor.
    destruct unfold_list_option; try congruence. destruct eval_term; try congruence.
    firstorder.
  - intros f i o s1 T U s2 HTU Hint.
    inversion Hint; subst; simpl in *; now econstructor.
  - intros Ss V H s1 T U s2 HTU Hint.
     inversion Hint; subst; simpl in *.
     econstructor; try eassumption. eapply Forall_forall in H; [eapply H|]; eassumption.
  - intros S H s1 T U r1 HTU Hint. revert s1 Hint. induction S; intros s1 Hint.
    inversion Hint; subst; simpl in *; constructor.
    inversion Hint; subst; simpl in *. 
    econstructor; [eapply (proj1(Forall_forall _ _)H); [now left|eassumption..]|]. 
    now eapply IHS; [inversion H|].
Qed.

Definition interpBC_inc l bi fi iFC := proj1 (interp_inc l bi fi iFC).
Definition interpC_inc l bi fi iFC := proj2 (interp_inc l bi fi iFC).


Lemma Scons_inc:
  forall n (x:concrete_quadruple st bis fis),
  consequence st bis fis SinterpFC n x ->
  consequence st bis fis SinterpFC (S n) x.
Proof.
  induction n.
  - simpl; intros x H; destruct H.
  - intros x H; simpl in H; inversion H; destruct h; subst; simpl;
      eapply H_intro; try eauto; eapply interpC_inc; eassumption. 
Qed.
Lemma Acons_inc:
  forall n (x:concrete_quadruple ar bia fia),
  consequence ar bia fia AinterpFC n x ->
  consequence ar bia fia AinterpFC (S n) x.
Proof.
  induction n.
  - simpl; intros x H; destruct H.
  - intros x H; simpl in H; inversion H; destruct h; subst; simpl;
      eapply H_intro; try eauto; eapply interpC_inc; eassumption. 
Qed.

Lemma Scons_inc_str:
  forall m n x, m <= n ->
  consequence st bis fis SinterpFC m x ->
  consequence st bis fis SinterpFC n x.
Proof.
  intros ? ? ? H ?; induction H; [|apply Scons_inc]; trivial. 
Qed.
Lemma Acons_inc_str:
  forall m n x, m <= n ->
  consequence ar bia fia AinterpFC m x ->
  consequence ar bia fia AinterpFC n x.
Proof.
  intros ? ? ? H ?; induction H; [|apply Acons_inc]; trivial. 
Qed.

Lemma invS_nil:
  forall l bi fi iFC s1 e,
  interpC l bi fi iFC [] s1 e ->
  exists T, s1 = (e, T).
Proof.
  intros l bi fi iFC s1 e H; inversion H as [|? T]; subst.
  now exists T.
Qed.

Lemma invS_cons:
  forall l bi fi iFC B S s1 T e,
  interpC l bi fi iFC (B::S) (s1,T) e ->
  exists s2,
  interpBC l bi fi iFC B (s1,T) s2 /\
  interpC l bi fi iFC S (s2,T) e.
Proof.
  intros l bi fi iFC B S s1 T e H; inversion H as [B' S' s1' s2 e' A C  |]; subst.
  exists s2; firstorder.
Qed.
  
Lemma invF:
  forall l bi fi iFC f l1 l2 s1 s2,
  interpBC l bi fi iFC (F _ _ _ _ _ _ _ f l1 l2) s1 s2 ->
  exists e1 T lv,
  s1 = (e1, T) /\
  s2 = add_asn l bi fi e1 l2 lv /\
  interpFC_opt l iFC f (map (find l bi fi e1) (map (skel_name _ _ _)l1)) lv.
Proof.
  intros; inversion H; repeat eexists; assumption.
Qed. 

Lemma invH l bi fi:
  forall h iFC l1 l2 s1 s2 (t:typed_term _ _ _ _ _),
  interpBC l bi fi iFC (H _ _ _ _ _ _ _ h l1 t l2) s1 s2 ->
  exists l1' e1 T v t',
  s1 = (e1, T) /\
  s2 = add_asn _ bi fi e1 l2 v /\
  Some l1' = unfold_list_option (map (find l bi fi e1) (map (skel_name _ _ _) l1)) /\
  Some t' = eval_term l bi fi e1 (projT2 t) /\
  T (h, l1', t', v).
Proof.
  intros h iFC l1 l2 s1 s2 t H.
  inversion H as [|h' e1 T xf1 xf2 t0 v A|]; subst.
  remember(unfold_list_option (map (find l bi fi e1) (map (skel_name _ _ _) l1))) as o.
  remember(eval_term l bi fi e1 (projT2 t)) as p.
  destruct o as [l1'|]; destruct p as [t' |]; try (destruct A; fail).
  exists l1', e1, T, v, t'; firstorder.
Qed. 

Ltac breakhyp H' :=
  simpl in H'; match type of H' with
  | interpC _ _ _ _ [] _ _ =>
      apply invS_nil in H'; destruct H' as [? ?]; subst_ext
  | interpC _ _ _ _ (_::_) _ _ =>
      apply invS_cons in H'; let A := fresh in let x := fresh in let B := fresh in
      destruct H' as [x [A B]]; try (injection x as ? ?); breakhyp A; breakhyp B; subst_ext
  | interpBC _ _ _ _ (F _ _ _ _ _ _ _ _ _ _) _ _ =>
      let a := fresh in let b := fresh in
      apply invF in H'; destruct H' as [? [? [a [b [? ?]]]]];
      try (injection a as ? ?); try injection b as ? ?; subst_ext
  | interpBC _ _ _ _ (H _ _ _ _ _ _ _ _ _ _ _) _ _ =>
      let q := fresh in let w := fresh in
      apply invH in H'; destruct H' as [? [? [? [? [? [q [w [? [? ?]]]]]]]]];
      try (injection q as ? ?); try injection w as ? ?; subst_ext
  | immediate_consequence _ _ _ _ _ _=> inversion H'; clear H'; subst_ext
  | _ => fail "The hypothesis is not of a fitting type"      
  end.

Ltac breakgoal :=
  simpl; match goal with
  | [ |- interpC _ _ _ _ [] _ _ ] =>
      econstructor
  | [ |- interpC _ _ _ _ (_::_) _ _ ] =>
      econstructor; breakgoal
  | [ |- interpBC _ _ _ _ (F _ _ _ _ _ _ _ _ _ _) _ _ ] =>
      econstructor
  | [ |- interpBC _ _ _ _ (H _ _ _ _ _ _ _ _ _ _ _) _ _ ] =>
      econstructor
  | [ |- immediate_consequence _ _ _ _ _ _ ] => econstructor
  end.

Tactic Notation "break" hyp(H) :=
  breakhyp H.
Tactic Notation "break" "in" hyp(H) :=
  breakhyp H.
Tactic Notation "break" :=
  breakgoal.

Ltac destructif A :=
  match goal with | [|- context [if ?x then _ else _ ] ] => destruct x eqn : A end.

Ltac destructifin A B :=
  match type of A with | context [if ?x then _ else _ ] => destruct x eqn : B in A end.

Tactic Notation "destructif" "as" ident(A):= destructif (A).
Tactic Notation "destructif" := let x := fresh "H" in destructif as x.
Tactic Notation "destructif" "in" hyp(H) "as" ident(A) := destructifin H A.
Tactic Notation "destructif" "as" ident(A) "in" hyp(H) := destructifin H A.
Tactic Notation "destructif" "in" hyp(H) := let x := fresh "H" in destructif in H as x.

Ltac sort H':=
  match type of H' with
  | SinterpFC S.f_add _ _ =>
      let x:= fresh in let H := fresh in
      decompose [ex and] (AddStackSort _ _ H'); subst_ext
  | SinterpFC S.f_sub _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (SubStackSort _ _ H'); subst_ext
  | SinterpFC S.f_mul _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (MulStackSort _ _ H'); subst_ext
  | SinterpFC S.f_div _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (DivStackSort _ _ H'); subst_ext
  | SinterpFC S.f_litToVal _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (LitToValStackSort _ _ H'); subst_ext
  | SinterpFC S.f_push _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (PushStackSort _ _ H'); subst_ext
  | SinterpFC S.f_pop _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (PopStackSort _ _ H'); subst_ext
  | AinterpFC A.f_add _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (AddArithmeticSort _ _ H'); subst_ext
  | AinterpFC A.f_sub _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (SubArithmeticSort _ _ H'); subst_ext
  | AinterpFC A.f_mul _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (MulArithmeticSort _ _ H'); subst_ext
  | AinterpFC A.f_div _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (DivArithmeticSort _ _ H'); subst_ext
  | AinterpFC A.f_litToVal _ _ =>
      let x := fresh in let H := fresh in
      decompose [ex and] (LitToValArithmeticSort _ _ H'); subst_ext
  | _ => idtac
  end.

