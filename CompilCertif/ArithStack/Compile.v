Require Arithmetic Stack.
Require Import List Ascii String.
Require Import Semantics Concrete.
Require Import Prelim.
Import ListNotations.
Open Scope string_scope.


Notation "x <- y ; z" := (option_bind y (fun x => z))
    (right associativity, at level 55, y at level 53).

(* Définition du compilateur *)
Fixpoint compile (e : cterm ar bia) : option(cterm st bis):=
  match e with
  | cterm_base _ _ A.s_literal n => None
  | cterm_constructor _ _ c l =>
      match c, l with | A.c_Const, [cterm_base _ _ A.s_literal n] =>
          Some (cterm_constructor st bis S.c_Push
            [cterm_base st bis S.s_literal (castLiteral n)])
      | A.c_Plus, [e1 ; e2] =>
          e'1 <- (compile e1);
          e'2 <- (compile e2);
          Some (cterm_constructor st bis S.c_Block
            [ cterm_constructor st bis S.c_Block [e'1; e'2];
              cterm_constructor st bis S.c_Plus []] )
      | A.c_Minus, [e1 ; e2] =>
          e'1 <- (compile e1);
          e'2 <- (compile e2);
          Some (cterm_constructor st bis S.c_Block
            [ cterm_constructor st bis S.c_Block [e'1; e'2];
              cterm_constructor st bis S.c_Minus []] )
      | A.c_Times, [e1 ; e2] =>
          e'1 <- (compile e1);
          e'2 <- (compile e2);
          Some (cterm_constructor st bis S.c_Block
            [ cterm_constructor st bis S.c_Block [e'1; e'2];
              cterm_constructor st bis S.c_Times []] )
      | A.c_Div, [e1 ; e2] =>
          e'1 <- (compile e1);
          e'2 <- (compile e2);
          Some (cterm_constructor st bis S.c_Block
            [ cterm_constructor st bis S.c_Block [e'1; e'2];
              cterm_constructor st bis S.c_Div []] )
      | _,_ => None
      end
  end.

(* Lemma compile_wf :
  forall e, compile e = None <-> (wellFormed _ e) <> Some(Prgm _ _ _ A.s_expr).
Proof.
  apply cterm_rec.
  - intros [] f; simpl; split; [intros _ b; congruence|tauto].
  - intros l constr IH. split.
    + intro A. destruct constr.
      * destruct l as [|c [|? ?]]; simpl in A.
        -- intros H; simpl in H; congruence.
        -- destruct c as [[]|]; [simpl|simpl; destructif];try discriminate.
        injection e as e'. clear e H.
           destruct l; simpl in e'; destructif in e'; discriminate.
        -- intros F; simpl; simpl in F; destructif in F; congruence.
      * destruct l; [|destruct l]; [| |destruct l].
        -- intros H; simpl in H; congruence.
        -- intros H; simpl in H; try congruence; destruct sumbool_rec;
            congruence. 
        -- intro. simpl in H; destructif in H as cc; clear cc; try discriminate.
           injection e as H1 H2. destruct(compile c) eqn:e1; destruct(compile c0) eqn:e2;
           simpl in A; rewrite e1, e2 in A; simpl in A.
           ++ discriminate.
           ++ apply IH in e2; [now apply e2|right; now left].
           ++ apply IH in e1; [now apply e1|now left].
           ++ apply IH in e1; [now apply e1|now left].
        -- simpl. destructif as cc; clear cc; [|intros H]; discriminate.
      * destruct l; [|destruct l]; [| |destruct l].
        -- intros H; simpl in H; congruence.
        -- intros H; simpl in H; try congruence; destruct sumbool_rec;
            congruence. 
        -- intro. simpl in H; destructif in H as cc; clear cc; try discriminate.
           injection e as H1 H2. destruct(compile c) eqn:e1; destruct(compile c0) eqn:e2;
           simpl in A; rewrite e1, e2 in A; simpl in A.
           ++ discriminate.
           ++ apply IH in e2; [now apply e2|right; now left].
           ++ apply IH in e1; [now apply e1|now left].
           ++ apply IH in e1; [now apply e1|now left].
        -- simpl. destructif as cc; clear cc; [|intros H]; discriminate.
      * destruct l; [|destruct l]; [| |destruct l].
        -- intros H; simpl in H; congruence.
        -- intros H; simpl in H; try congruence; destruct sumbool_rec;
            congruence. 
        -- intro. simpl in H; destructif in H as cc; clear cc; try discriminate.
           injection e as H1 H2. destruct(compile c) eqn:e1; destruct(compile c0) eqn:e2;
           simpl in A; rewrite e1, e2 in A; simpl in A.
           ++ discriminate.
           ++ apply IH in e2; [now apply e2|right; now left].
           ++ apply IH in e1; [now apply e1|now left].
           ++ apply IH in e1; [now apply e1|now left].
        -- simpl. destructif as cc; clear cc; [|intros H]; discriminate.
      * destruct l; [|destruct l]; [| |destruct l].
        -- intros H; simpl in H; congruence.
        -- intros H; simpl in H; try congruence; destruct sumbool_rec;
            congruence. 
        -- intro. simpl in H; destructif in H as cc; clear cc; try discriminate.
           injection e as H1 H2. destruct(compile c) eqn:e1; destruct(compile c0) eqn:e2;
           simpl in A; rewrite e1, e2 in A; simpl in A.
           ++ discriminate.
           ++ apply IH in e2; [now apply e2|right; now left].
           ++ apply IH in e1; [now apply e1|now left].
           ++ apply IH in e1; [now apply e1|now left].
        -- simpl. destructif as cc; clear cc; [|intros H]; discriminate.
    + intro A. destruct constr.
      -- destruct l as [|[[] n|] [|? l']]; now simpl.
      -- destruct l as [|a [|b [|? l']]]; simpl; try easy.
         destruct (compile a) as [a'|] eqn: ca; destruct (compile b) as [b'|]
             eqn: cb; [|reflexivity..]; simpl. destruct A.
        enough(HA:wellFormed _ a = Some (Prgm _ _ _ Arithmetic.s_expr)).
        enough(HB:wellFormed _ b = Some (Prgm _ _ _ Arithmetic.s_expr)).
        simpl. rewrite HA, HB. simpl. trivial.
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite cb in F; discriminate|right; now left].
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite ca in F; discriminate|now left].
      -- destruct l as [|a [|b [|? l']]]; simpl; try easy.
         destruct (compile a) as [a'|] eqn: ca; destruct (compile b) as [b'|]
             eqn: cb; [|reflexivity..]; simpl. destruct A.
        enough(HA:wellFormed _ a = Some (Prgm _ _ _ Arithmetic.s_expr)).
        enough(HB:wellFormed _ b = Some (Prgm _ _ _ Arithmetic.s_expr)).
        simpl. rewrite HA, HB. simpl. trivial.
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite cb in F; discriminate|right; now left].
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite ca in F; discriminate|now left].
      -- destruct l as [|a [|b [|? l']]]; simpl; try easy.
         destruct (compile a) as [a'|] eqn: ca; destruct (compile b) as [b'|]
             eqn: cb; [|reflexivity..]; simpl. destruct A.
        enough(HA:wellFormed _ a = Some (Prgm _ _ _ Arithmetic.s_expr)).
        enough(HB:wellFormed _ b = Some (Prgm _ _ _ Arithmetic.s_expr)).
        simpl. rewrite HA, HB. simpl. trivial.
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite cb in F; discriminate|right; now left].
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite ca in F; discriminate|now left].
      -- destruct l as [|a [|b [|? l']]]; simpl; try easy.
         destruct (compile a) as [a'|] eqn: ca; destruct (compile b) as [b'|]
             eqn: cb; [|reflexivity..]; simpl. destruct A.
        enough(HA:wellFormed _ a = Some (Prgm _ _ _ Arithmetic.s_expr)).
        enough(HB:wellFormed _ b = Some (Prgm _ _ _ Arithmetic.s_expr)).
        simpl. rewrite HA, HB. simpl. trivial.
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite cb in F; discriminate|right; now left].
        apply NNPP; [apply option_eq_dec, sort_eq_dec|]. intro F.
        apply IH in F; [rewrite ca in F; discriminate|now left].
Qed. *)

Theorem compile_k_complete :
  forall k e n1 s h1 (s2 ns2: value st bis fis),
    compile e = Some s ->
    SinterpFC Stack.f_push [val_flow st bis fis _ (castValue n1); s2] [ns2] ->
    consequence ar bia fia AinterpFC k (h1, [], e, [val_flow ar bia fia _ n1]) -> 
    exists h2, concrete_semantics st bis fis SinterpFC (h2, [s2], s, [ns2]).
Proof.
  intro k0. apply(Wf_nat.lt_wf_rec k0).
  intros k IHk. destruct e as [|];
      intros n1 s h1 s2 ns2 Hc Hp Hs; destruct h1; exists S.h_eval.
  - simpl in Hc; destruct b; congruence. 
  - destruct l0; [|destruct l0]; [| |destruct l0] ;
    try (destruct l; simpl in Hc; congruence).
    + destruct s as [s f|].
      * destruct l; destruct s; destruct c; simpl in Hc; try destruct b; congruence.
      * destruct l; simpl in Hc; try congruence. clear IHk. destruct c; try congruence.
        destruct b; try (inversion Hc; subst; clear Hc). exists 1. simpl.
        destruct(PushStackSort _ _ Hp) as (a, (b, (c, ?))).
        inversion H as [A B]; inversion A; inversion B; subst; clear A B H.
        destruct(StackLitToValDef (castLiteral b0)) as [x HH].
        destruct(StackPushDef x b) as [x' H'].
        break; simpl; [firstorder..| |]. break; simpl.
        exact HH. simpl. exact H'. simpl. repeat f_equal.
        enough(castValue n1 = x); subst. apply(StackPushFun _ _ _ _ H' Hp).
        destruct(sameValue) as (_, surj); destruct (surj x) as (n2, ?); subst.
        apply (StackLitToValFun (castLiteral b0) _ _);
          [clear Hp HH surj H' n2 b c k0| exact HH]. 
        destruct k; [destruct Hs|]. simpl in Hs. inversion Hs; subst.
        simpl in *. repeat destruct H3 as [H3|H3]; inversion H3. subst.
        clear H3. break H6. simpl in *. sort H4. apply sameLitToVal. assumption.
    + destruct l; simpl in Hc.
      * destruct c; try destruct b; congruence. 
      * destruct k; simpl in Hs; [destruct Hs|]. inversion Hs; subst_ext; clear Hs.
        repeat (destruct H3 as [H3|H3]; subst_ext).
        remember (compile c) as e0; destruct e0; simpl in Hc; try (subst; congruence).
        remember (compile c0) as e1; destruct e1; simpl in Hc; try (subst; congruence).
        subst_ext. rename c into e0, c0 into e1, c1 into c0, c2 into c1.
        break H6. subst_ext. destruct x2 as [|? [|? ?]]; subst_ext. simpl in *.
        destruct x3 as [|? [|? ?]]; subst_ext; simpl in H8. sort H8. sort Hp.
        apply sameDiv in H8. destruct(StackPushDef (castValue x) x2) as [xx2 ?].
        eapply IHk in H5; [| constructor | symmetry|]; [| eassumption..].
        destruct(StackPushDef (castValue x0) xx2) as [x0xx2 ?]. 
        eapply IHk in H6; [| constructor | symmetry|]; [| eassumption..].
        destruct H5 as [h1 [m1 ?]]. destruct H6 as [h2 [m2 H9]]. destruct h1. destruct h2.
        set(m := max m1 m2). eapply (Scons_inc_str _ m) in H1; [|apply Max.le_max_l].
        eapply (Scons_inc_str _ m) in H9; [|apply Max.le_max_r]. clearbody m; clear m1 m2.
        exists(S (S m)). econstructor; simpl; try tauto. break; simpl.
        econstructor; simpl; try tauto. break; simpl; try eauto; simpl; try eauto; simpl.
        reflexivity. simpl. econstructor; simpl; try tauto. break; simpl.
        apply StackPushPopDef. eassumption. simpl. apply StackPushPopDef. eassumption. simpl.
        eassumption. simpl. eassumption. simpl. reflexivity. simpl. reflexivity.
      * destruct k; simpl in Hs; [destruct Hs|]. inversion Hs; subst_ext; clear Hs.
        repeat (destruct H3 as [H3|H3]; subst_ext).
        remember (compile c) as e0; destruct e0; simpl in Hc; try (subst; congruence).
        remember (compile c0) as e1; destruct e1; simpl in Hc; try (subst; congruence).
        subst_ext. rename c into e0, c0 into e1, c1 into c0, c2 into c1.
        break H6. subst_ext. destruct x2 as [|? [|? ?]]; subst_ext. simpl in *.
        destruct x3 as [|? [|? ?]]; subst_ext; simpl in H8. sort H8. sort Hp.
        apply sameSub in H8. destruct(StackPushDef (castValue x) x2) as [xx2 ?].
        eapply IHk in H5; [| constructor | symmetry|]; [| eassumption..].
        destruct(StackPushDef (castValue x0) xx2) as [x0xx2 ?]. 
        eapply IHk in H6; [| constructor | symmetry|]; [| eassumption..].
        destruct H5 as [h1 [m1 ?]]. destruct H6 as [h2 [m2 H9]]. destruct h1. destruct h2.
        set(m := max m1 m2). eapply (Scons_inc_str _ m) in H1; [|apply Max.le_max_l].
        eapply (Scons_inc_str _ m) in H9; [|apply Max.le_max_r]. clearbody m; clear m1 m2.
        exists(S (S m)). econstructor; simpl; try tauto. break; simpl.
        econstructor; simpl; try tauto. break; simpl; try eauto; simpl; try eauto; simpl.
        reflexivity. simpl. econstructor; simpl; try tauto. break; simpl.
        apply StackPushPopDef. eassumption. simpl. apply StackPushPopDef. eassumption. simpl.
        eassumption. simpl. eassumption. simpl. reflexivity. simpl. reflexivity.
      * destruct k; simpl in Hs; [destruct Hs|]. inversion Hs; subst_ext; clear Hs.
        repeat (destruct H3 as [H3|H3]; subst_ext).
        remember (compile c) as e0; destruct e0; simpl in Hc; try (subst; congruence).
        remember (compile c0) as e1; destruct e1; simpl in Hc; try (subst; congruence).
        subst_ext. rename c into e0, c0 into e1, c1 into c0, c2 into c1.
        break H6. subst_ext. destruct x2 as [|? [|? ?]]; subst_ext. simpl in *.
        destruct x3 as [|? [|? ?]]; subst_ext; simpl in H8. sort H8. sort Hp.
        apply sameAdd in H8. destruct(StackPushDef (castValue x) x2) as [xx2 ?].
        eapply IHk in H5; [| constructor | symmetry|]; [| eassumption..].
        destruct(StackPushDef (castValue x0) xx2) as [x0xx2 ?]. 
        eapply IHk in H6; [| constructor | symmetry|]; [| eassumption..].
        destruct H5 as [h1 [m1 ?]]. destruct H6 as [h2 [m2 H9]]. destruct h1. destruct h2.
        set(m := max m1 m2). eapply (Scons_inc_str _ m) in H1; [|apply Max.le_max_l].
        eapply (Scons_inc_str _ m) in H9; [|apply Max.le_max_r]. clearbody m; clear m1 m2.
        exists(S (S m)). econstructor; simpl; try tauto. break; simpl.
        econstructor; simpl; try tauto. break; simpl; try eauto; simpl; try eauto; simpl.
        reflexivity. simpl. econstructor; simpl; try tauto. break; simpl.
        apply StackPushPopDef. eassumption. simpl. apply StackPushPopDef. eassumption. simpl.
        eassumption. simpl. eassumption. simpl. reflexivity. simpl. reflexivity.
      * destruct k; simpl in Hs; [destruct Hs|]. inversion Hs; subst_ext; clear Hs.
        repeat (destruct H3 as [H3|H3]; subst_ext).
        remember (compile c) as e0; destruct e0; simpl in Hc; try (subst; congruence).
        remember (compile c0) as e1; destruct e1; simpl in Hc; try (subst; congruence).
        subst_ext. rename c into e0, c0 into e1, c1 into c0, c2 into c1.
        break H6. subst_ext. destruct x2 as [|? [|? ?]]; subst_ext. simpl in *.
        destruct x3 as [|? [|? ?]]; subst_ext; simpl in H8. sort H8. sort Hp.
        apply sameMul in H8. destruct(StackPushDef (castValue x) x2) as [xx2 ?].
        eapply IHk in H5; [| constructor | symmetry|]; [| eassumption..].
        destruct(StackPushDef (castValue x0) xx2) as [x0xx2 ?]. 
        eapply IHk in H6; [| constructor | symmetry|]; [| eassumption..].
        destruct H5 as [h1 [m1 ?]]. destruct H6 as [h2 [m2 H9]]. destruct h1. destruct h2.
        set(m := max m1 m2). eapply (Scons_inc_str _ m) in H1; [|apply Max.le_max_l].
        eapply (Scons_inc_str _ m) in H9; [|apply Max.le_max_r]. clearbody m; clear m1 m2.
        exists(S (S m)). econstructor; simpl; try tauto. break; simpl.
        econstructor; simpl; try tauto. break; simpl; try eauto; simpl; try eauto; simpl.
        reflexivity. simpl. econstructor; simpl; try tauto. break; simpl.
        apply StackPushPopDef. eassumption. simpl. apply StackPushPopDef. eassumption. simpl.
        eassumption. simpl. eassumption. simpl. reflexivity. simpl. reflexivity.
    + destruct l; simpl in Hc; try destruct c; try destruct b; congruence.
Qed.

Theorem compile_k_correct :
  forall k e s h1 s2 ns2,
    compile e = Some s ->
    consequence st bis fis SinterpFC k (h1, [s2], s, [ns2]) ->
    exists h2 n, 
      SinterpFC Stack.f_push [val_flow st bis fis _ (castValue n); s2] [ns2] /\
      concrete_semantics ar bia fia AinterpFC (h2, [], e, [val_flow ar bia fia _ n]).
Proof.
  intro k0.
  apply(Wf_nat.lt_wf_rec k0); intros k IHk; destruct e; intros s h1 s2 ns2 Hc Hp.
  - destruct b; simpl in Hc; inversion Hc.
  - destruct k; simpl in Hp; try (destruct Hp; fail). destruct h1. exists A.h_eval.
    destruct l; simpl in Hc.
    + destruct l0 as [|i [|? ?]]; try congruence; simpl in Hc;
      destruct i as [[]| ?]; try congruence.
      inversion Hc; subst; clear Hc.
      inversion Hp; subst; clear Hp.
      repeat (destruct H3 as [H3|H3]; subst_ext). break H6; subst_ext; simpl in *.
      sort H4. simpl in *. sort H6. destruct sameValue as [_ H]; decompose [ex] (H x).
      subst_ext; clear H. exists x0. split; [assumption|]. exists 1. simpl.
      econstructor; simpl; try eauto; [break; simpl; apply sameLitToVal; eauto|eauto]. 
    + destruct l0 as [|e1 [|e2 [|? ?]]]; try congruence.
      remember (compile e1) as c1; remember (compile e2) as c2.
      destruct c1 as [c1|]; destruct c2 as [c2|]; subst_ext.
      symmetry in Heqc1; symmetry in Heqc2; break Hp.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H6.
      destruct x2 as [|n1n2s2 [|? ?]]; subst_ext.
      destruct x3 as [|? [|? ?]]; subst_ext.
      destruct k as [|k]; subst_ext. break H5.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H8.
      destruct x2 as [|n1s2 [|? ?]]; subst_ext.
      destruct x3 as [|n2n1s2 [|? ?]]; subst_ext. 
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc1 H5).
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc2 H7).
      clear H5 H7. break H6. repeat destruct H8 as [H8|H8]; subst_ext; break H11.
      simpl in *. sort H. sort H0. sort H8. simpl in *. sort H10. simpl in *. sort H12.
      simpl in *. sort H13. destruct sameValue as [inj surj].
      destruct (surj x8); destruct (surj x11); destruct (surj x12); subst_ext.
      apply sameDiv in H13. exists x13. subst.
      assert(r1:=StackPushPop _ _ _ H0 H8); subst_ext.
      apply inj in H2; subst_ext; clear H8.
      assert(r1:=StackPushPop _ _ _ H H10); subst_ext.
      apply inj in H2; subst_ext; clear H10.
      simpl. destruct H3 as [n1 ?]. destruct H1 as [n2 ?].
      set(n := max n1 n2). destruct x; destruct x1.
      apply (Acons_inc_str n1 n) in H2; [|now apply Max.le_max_l];
      apply (Acons_inc_str n2 n) in H1; [|now apply Max.le_max_r].
      destruct H9 as [|? [|? ?]]; subst_ext. simpl in *.
      clearbody n; clear n1 n2. split; [assumption|]. exists(S n).
      simpl. econstructor; simpl; [eauto|eauto|..|].
      -- break;[simpl; eassumption|..]; [simpl; eassumption|..]; simpl; eassumption.
      -- simpl. reflexivity.
    + destruct l0 as [|e1 [|e2 [|? ?]]]; try congruence.
      remember (compile e1) as c1; remember (compile e2) as c2.
      destruct c1 as [c1|]; destruct c2 as [c2|]; subst_ext.
      symmetry in Heqc1; symmetry in Heqc2; break Hp.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H6.
      destruct x2 as [|n1n2s2 [|? ?]]; subst_ext.
      destruct x3 as [|? [|? ?]]; subst_ext.
      destruct k as [|k]; subst_ext. break H5.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H8.
      destruct x2 as [|n1s2 [|? ?]]; subst_ext.
      destruct x3 as [|n2n1s2 [|? ?]]; subst_ext. 
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc1 H5).
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc2 H7).
      clear H5 H7. break H6. repeat destruct H8 as [H8|H8]; subst_ext; break H11.
      simpl in *. sort H. sort H0. sort H8. simpl in *. sort H10. simpl in *. sort H12.
      simpl in *. sort H13. destruct sameValue as [inj surj].
      destruct (surj x8); destruct (surj x11); destruct (surj x12); subst_ext.
      apply sameSub in H13. exists x13. subst.
      assert(r1:=StackPushPop _ _ _ H0 H8); subst_ext.
      apply inj in H2; subst_ext; clear H8.
      assert(r1:=StackPushPop _ _ _ H H10); subst_ext.
      apply inj in H2; subst_ext; clear H10.
      simpl. destruct H3 as [n1 ?]. destruct H1 as [n2 ?].
      set(n := max n1 n2). destruct x; destruct x1.
      apply (Acons_inc_str n1 n) in H2; [|now apply Max.le_max_l];
      apply (Acons_inc_str n2 n) in H1; [|now apply Max.le_max_r].
      destruct H9 as [|? [|? ?]]; subst_ext. simpl in *.
      clearbody n; clear n1 n2. split; [assumption|]. exists(S n).
      simpl. econstructor; simpl; [eauto|eauto|..|].
      -- break;[simpl; eassumption|..]; [simpl; eassumption|..]; simpl; eassumption.
      -- simpl. reflexivity.
    + destruct l0 as [|e1 [|e2 [|? ?]]]; try congruence.
      remember (compile e1) as c1; remember (compile e2) as c2.
      destruct c1 as [c1|]; destruct c2 as [c2|]; subst_ext.
      symmetry in Heqc1; symmetry in Heqc2; break Hp.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H6.
      destruct x2 as [|n1n2s2 [|? ?]]; subst_ext.
      destruct x3 as [|? [|? ?]]; subst_ext.
      destruct k as [|k]; subst_ext. break H5.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H8.
      destruct x2 as [|n1s2 [|? ?]]; subst_ext.
      destruct x3 as [|n2n1s2 [|? ?]]; subst_ext. 
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc1 H5).
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc2 H7).
      clear H5 H7. break H6. repeat destruct H8 as [H8|H8]; subst_ext; break H11.
      simpl in *. sort H. sort H0. sort H8. simpl in *. sort H10. simpl in *. sort H12.
      simpl in *. sort H13. destruct sameValue as [inj surj].
      destruct (surj x8); destruct (surj x11); destruct (surj x12); subst_ext.
      apply sameAdd in H13. exists x13. subst.
      assert(r1:=StackPushPop _ _ _ H0 H8); subst_ext.
      apply inj in H2; subst_ext; clear H8.
      assert(r1:=StackPushPop _ _ _ H H10); subst_ext.
      apply inj in H2; subst_ext; clear H10.
      simpl. destruct H3 as [n1 ?]. destruct H1 as [n2 ?].
      set(n := max n1 n2). destruct x; destruct x1.
      apply (Acons_inc_str n1 n) in H2; [|now apply Max.le_max_l];
      apply (Acons_inc_str n2 n) in H1; [|now apply Max.le_max_r].
      destruct H9 as [|? [|? ?]]; subst_ext. simpl in *.
      clearbody n; clear n1 n2. split; [assumption|]. exists(S n).
      simpl. econstructor; simpl; [eauto|eauto|..|].
      -- break;[simpl; eassumption|..]; [simpl; eassumption|..]; simpl; eassumption.
      -- simpl. reflexivity.
    + destruct l0 as [|e1 [|e2 [|? ?]]]; try congruence.
      remember (compile e1) as c1; remember (compile e2) as c2.
      destruct c1 as [c1|]; destruct c2 as [c2|]; subst_ext.
      symmetry in Heqc1; symmetry in Heqc2; break Hp.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H6.
      destruct x2 as [|n1n2s2 [|? ?]]; subst_ext.
      destruct x3 as [|? [|? ?]]; subst_ext.
      destruct k as [|k]; subst_ext. break H5.
      simpl in H3; decompose [or] H3; clear H3; subst_ext; break H8.
      destruct x2 as [|n1s2 [|? ?]]; subst_ext.
      destruct x3 as [|n2n1s2 [|? ?]]; subst_ext. 
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc1 H5).
      decompose [and ex] (IHk k (ltac:(repeat constructor)) _ _ _ _ _ Heqc2 H7).
      clear H5 H7. break H6. repeat destruct H8 as [H8|H8]; subst_ext; break H11.
      simpl in *. sort H. sort H0. sort H8. simpl in *. sort H10. simpl in *. sort H12.
      simpl in *. sort H13. destruct sameValue as [inj surj].
      destruct (surj x8); destruct (surj x11); destruct (surj x12); subst_ext.
      apply sameMul in H13. exists x13. subst.
      assert(r1:=StackPushPop _ _ _ H0 H8); subst_ext.
      apply inj in H2; subst_ext; clear H8.
      assert(r1:=StackPushPop _ _ _ H H10); subst_ext.
      apply inj in H2; subst_ext; clear H10.
      simpl. destruct H3 as [n1 ?]. destruct H1 as [n2 ?].
      set(n := max n1 n2). destruct x; destruct x1.
      apply (Acons_inc_str n1 n) in H2; [|now apply Max.le_max_l];
      apply (Acons_inc_str n2 n) in H1; [|now apply Max.le_max_r].
      destruct H9 as [|? [|? ?]]; subst_ext. simpl in *.
      clearbody n; clear n1 n2. split; [assumption|]. exists(S n).
      simpl. econstructor; simpl; [firstorder|..].
      -- break;[simpl; eassumption|..]; [simpl; eassumption|..]; simpl; eassumption.
      -- simpl. reflexivity.
Qed.

Inductive StateRel : list (value ar bia fia) -> list (value ar bia fia) ->
                     list (value st bis fis) -> list (value st bis fis) -> Prop :=
| SR_intro: forall n s ns, SinterpFC S.f_push [val_flow st bis fis _ (castValue n); s] [ns] ->
    StateRel [] [val_flow ar bia fia _ n] [s] [ns].

Theorem compile_spec:
  forall i1 o1 i2 o2, StateRel i1 o1 i2 o2 ->
  forall e e', compile e = Some e' ->
    (exists h, concrete_semantics ar bia fia AinterpFC (h, i1, e, o1)) <->
    exists h, concrete_semantics st bis fis SinterpFC (h, i2, e', o2).
Proof.
  intros i1 o1 i2 o2 R e e' Hc; inversion R as [n s ns H]; subst_ext.
  split; intros (h,(k,A)).
  - eapply compile_k_complete; eassumption.
  - destruct(compile_k_correct k e e' h s ns Hc A) as [h2 [n' [H' cs]]]. 
    apply StackPushPopDef in H; apply StackPushPopDef in H'. 
    sort H. sort H'. destruct (StackPopFun _ _ _ _ _ H H').
    apply (proj1 sameValue) in H0; subst_ext. exists h2; exact cs.
Qed.


