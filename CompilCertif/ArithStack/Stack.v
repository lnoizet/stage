Require Import List Ascii String.
Import ListNotations.
Require Import Coq.Vectors.Fin.
Require Import ZArith Semantics.

Open Scope string_scope.

Set Decidable Equality Schemes.
Inductive constructor: Set :=
| c_Block | c_Div | c_Minus | c_Nop | c_Plus | c_Push | c_Times.

Inductive filter: Set :=
| f_add | f_div | f_litToVal | f_mul | f_pop | f_push | f_sub.

Inductive hook: Set :=
| h_eval.

Inductive base_sort: Set :=
| s_literal.

Inductive flow_sort: Set :=
| s_stack | s_value.

Inductive prgm_sort: Set :=
| s_stmt.

Module Notations1.
  Notation "'term_sort'" := (term_sort base_sort prgm_sort).
  Notation "'Base'" := (Base base_sort prgm_sort).
  Notation "'Prgm'" := (Prgm base_sort prgm_sort).
  Notation "'sort'" := (sort base_sort flow_sort prgm_sort).
  Notation "'Term'" := (Term base_sort flow_sort prgm_sort).
  Notation "'Flow'" := (Flow base_sort flow_sort prgm_sort).
End Notations1.
Import Notations1.

Definition csort c : (list term_sort * prgm_sort):=
  match c with
  | c_Block => ([Prgm s_stmt; Prgm s_stmt], s_stmt)
  | c_Div => ([], s_stmt)
  | c_Minus => ([], s_stmt)
  | c_Nop => ([], s_stmt)
  | c_Plus => ([], s_stmt)
  | c_Push => ([Base s_literal], s_stmt)
  | c_Times => ([], s_stmt)
  end.

Definition fsort (f : filter) : (list sort * list sort) :=
  match f with
  | f_add => ([Flow s_value; Flow s_value], [Flow s_value])
  | f_div => ([Flow s_value; Flow s_value], [Flow s_value])
  | f_litToVal => ([Term (Base s_literal)], [Flow s_value])
  | f_mul => ([Flow s_value; Flow s_value], [Flow s_value])
  | f_pop => ([Flow s_stack], [Flow s_value; Flow s_stack])
  | f_push => ([Flow s_value; Flow s_stack], [Flow s_stack])
  | f_sub => ([Flow s_value; Flow s_value], [Flow s_value])
  end.

Definition hsort (h: hook) : (list flow_sort * prgm_sort * list flow_sort) :=
  match h with
  | h_eval => ([s_stack], s_stmt, [s_stack])
  end.

Module Notations2.
  Notation "'bone'" := (bone constructor filter hook base_sort flow_sort prgm_sort csort).
  Notation "'F'" := (F constructor filter hook base_sort flow_sort prgm_sort csort).
  Notation "'B'" := (B constructor filter hook base_sort flow_sort prgm_sort csort).
  Notation "'H'" := (H constructor filter hook base_sort flow_sort prgm_sort csort).
  Notation "'skeleton'" := (skeleton constructor filter hook base_sort flow_sort prgm_sort csort).
  Notation "'skeletal_semantics'" := (skeletal_semantics constructor filter hook base_sort flow_sort prgm_sort csort).
  Notation "'term'" := (term constructor base_sort flow_sort prgm_sort csort).
  Notation "'term_sv'" := (term_sv constructor base_sort flow_sort prgm_sort csort _).
  Notation "'term_constructor'" := (term_constructor constructor base_sort flow_sort prgm_sort csort).
  Notation skel_var_intro := (skel_var_intro base_sort flow_sort prgm_sort).
  Notation skel_var := (skel_var base_sort flow_sort prgm_sort).
  Notation well_formed := (well_formed constructor filter hook base_sort flow_sort prgm_sort base_sort_eq_dec flow_sort_eq_dec prgm_sort_eq_dec csort fsort hsort).

  Notation "'svi'" := (fun t s => skel_var_intro t s).

  Notation "'tnil'" := (nil_term constructor base_sort flow_sort prgm_sort csort).
  Notation "a %% b" := (cons_term constructor base_sort flow_sort prgm_sort csort a b) (right associativity, at level 23).

  Notation "'tsvi'" := (fun t s => existT _ t (svi t s)).
  Notation "'tterm_sv'" := (fun s => existT _ _ (term_sv s)).
  Notation "'tterm_constructor'" := (fun c l => existT _ _ (term_constructor c l)).

End Notations2.
Import Notations2.

Definition semantics : skeletal_semantics :=
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Block,
    [tsvi (Term (Prgm s_stmt)) "xt1"; tsvi (Term (Prgm s_stmt)) "xt2"],
    [ H h_eval [tsvi (Flow s_stack) "x_s"] (tterm_sv (svi (Term (Prgm s_stmt)) "xt1")) [tsvi (Flow s_stack) "x_s'"] ;
      H h_eval [tsvi (Flow s_stack) "x_s'"] (tterm_sv (svi (Term (Prgm s_stmt)) "xt2")) [tsvi (Flow s_stack) "x_o"]],
    [tsvi (Flow s_stack) "x_o"]) ::
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Div,
    [],
    [ F f_pop [tsvi (Flow s_stack) "x_s"] [tsvi (Flow s_value) "xf1"; tsvi (Flow s_stack) "xs1"] ;
      F f_pop [tsvi (Flow s_stack) "xs1"] [tsvi (Flow s_value) "xf2"; tsvi (Flow s_stack) "xs2"] ;
      F f_div [tsvi (Flow s_value) "xf2"; tsvi (Flow s_value) "xf1"] [tsvi (Flow s_value) "xf3"] ;
      F f_push [tsvi (Flow s_value) "xf3"; tsvi (Flow s_stack) "xs2"] [tsvi (Flow s_stack) "x_o"]],
    [tsvi (Flow s_stack) "x_o"]) ::
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Minus,
    [],
    [ F f_pop [tsvi (Flow s_stack) "x_s"] [tsvi (Flow s_value) "xf1"; tsvi (Flow s_stack) "xs1"] ;
      F f_pop [tsvi (Flow s_stack) "xs1"] [tsvi (Flow s_value) "xf2"; tsvi (Flow s_stack) "xs2"] ;
      F f_sub [tsvi (Flow s_value) "xf2"; tsvi (Flow s_value) "xf1"] [tsvi (Flow s_value) "xf3"] ;
      F f_push [tsvi (Flow s_value) "xf3"; tsvi (Flow s_stack) "xs2"] [tsvi (Flow s_stack) "x_o"]],
    [tsvi (Flow s_stack) "x_o"]) ::
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Nop,
    [],
    [ ],
    [tsvi (Flow s_stack) "x_s"]) ::
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Plus,
    [],
    [ F f_pop [tsvi (Flow s_stack) "x_s"] [tsvi (Flow s_value) "xf1"; tsvi (Flow s_stack) "xs1"] ;
      F f_pop [tsvi (Flow s_stack) "xs1"] [tsvi (Flow s_value) "xf2"; tsvi (Flow s_stack) "xs2"] ;
      F f_add [tsvi (Flow s_value) "xf2"; tsvi (Flow s_value) "xf1"] [tsvi (Flow s_value) "xf3"] ;
      F f_push [tsvi (Flow s_value) "xf3"; tsvi (Flow s_stack) "xs2"] [tsvi (Flow s_stack) "x_o"]],
    [tsvi (Flow s_stack) "x_o"]) ::
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Push,
    [tsvi (Term (Base s_literal)) "xt1"],
    [ F f_litToVal [tsvi (Term (Base s_literal)) "xt1"] [tsvi (Flow s_value) "xf1"] ;
      F f_push [tsvi (Flow s_value) "xf1"; tsvi (Flow s_stack) "x_s"] [tsvi (Flow s_stack) "x_o"]],
    [tsvi (Flow s_stack) "x_o"]) ::
  ( h_eval, [tsvi (Flow s_stack) "x_s"],
    c_Times,
    [],
    [ F f_pop [tsvi (Flow s_stack) "x_s"] [tsvi (Flow s_value) "xf1"; tsvi (Flow s_stack) "xs1"] ;
      F f_pop [tsvi (Flow s_stack) "xs1"] [tsvi (Flow s_value) "xf2"; tsvi (Flow s_stack) "xs2"] ;
      F f_mul [tsvi (Flow s_value) "xf2"; tsvi (Flow s_value) "xf1"] [tsvi (Flow s_value) "xf3"] ;
      F f_push [tsvi (Flow s_value) "xf3"; tsvi (Flow s_stack) "xs2"] [tsvi (Flow s_stack) "x_o"]],
    [tsvi (Flow s_stack) "x_o"]) :: [].
(* Compute(well_formed semantics). *)

Definition lang := mklang constructor filter hook base_sort flow_sort prgm_sort base_sort_eq_dec flow_sort_eq_dec prgm_sort_eq_dec csort fsort hsort semantics.


