Require Import String.
Require Import ZArith.
Require Import List.
Import ListNotations.

Section Skeleton.
  Variable constructor: Type.
  Variable filter: Type.
  Variable hook: Type.
  Variable base_sort: Type.
  Variable flow_sort: Type.
  Variable prgm_sort: Type.
  Variable base_sort_eq_dec: forall x y: base_sort, {x = y} + {x <> y}.
  Variable flow_sort_eq_dec: forall x y: flow_sort, {x = y} + {x <> y}.
  Variable prgm_sort_eq_dec: forall x y: prgm_sort, {x = y} + {x <> y}.

  Inductive term_sort :=
  | Base : base_sort -> term_sort
  | Prgm : prgm_sort -> term_sort.

  Inductive sort :=
  | Term : term_sort -> sort
  | Flow : flow_sort -> sort.

  Definition term_sort_eq_dec (t1 t2: term_sort) : {t1 = t2} + {t1 <> t2}.
  Proof.
    destruct t1 as [b1|p1], t2 as [b2|p2]; try (right; congruence).
    - destruct (base_sort_eq_dec b1 b2); [left; now subst| right; congruence].
    - destruct (prgm_sort_eq_dec p1 p2); [left; now subst| right; congruence].
  Defined.

  Definition sort_eq_dec (s1 s2: sort) : {s1 = s2} + {s1 <> s2}.
  Proof.
    destruct s1 as [t1|f1], s2 as [t2|f2]; try (right; congruence).
    - destruct (term_sort_eq_dec t1 t2); [left; now subst| right; congruence].
    - destruct (flow_sort_eq_dec f1 f2); [left; now subst| right; congruence].
  Defined.

  Definition list_sort_eq_dec (l1 l2: list sort) : {l1 = l2} + {l1 <> l2} :=
    list_eq_dec sort_eq_dec l1 l2.

  Variable csort: constructor -> list term_sort * prgm_sort.
  Variable fsort: filter -> list sort * list sort.
  Variable hsort: hook -> (list flow_sort * prgm_sort) * list flow_sort.

  Inductive skel_var : sort -> Type :=
  | skel_var_intro : forall s, string -> skel_var s.
  Definition typed_skel_var := {s:sort & skel_var s}.

  Inductive term : term_sort -> Type :=
  | term_sv : forall t, skel_var (Term t) -> term t
  | term_constructor : forall (c:constructor),
      list_term (fst (csort c)) -> term (Prgm (snd (csort c)))
  with list_term: list term_sort -> Type :=
  | nil_term: list_term []
  | cons_term: forall {a A}, term a -> list_term A -> list_term (a::A).
  Definition typed_term := {s:term_sort & term s}.
  Definition typed_list_term := {l:list term_sort & list_term l}.

  Section TermAll.
    Variable (P: forall {s}, term s -> Type).
    Inductive All : forall {sl}, list_term sl -> Type :=
    | nil_all: @All [] nil_term
    | cons_all: forall {s t sl tl}, P s t -> @All sl tl -> @All (s::sl) (cons_term t tl).
  End TermAll.

  Section TermRect.
    Variable (P: forall {s}, term s -> Type).
    Variable sv: forall s n, P s (term_sv s n).
    Variable cons: forall (c:constructor) (l:list_term (fst (csort c))),
      All P l -> P _ (term_constructor c l).

    Fixpoint term_rect' s (t:term s): P s t:=
      match t with
      | term_sv s n => sv s n
      | term_constructor c l => cons c l ((fix list_term_rect s (lt:list_term s):All P lt :=
          match lt in list_term A return All P lt with
          | nil_term => nil_all P
          | cons_term t' rest => cons_all P (term_rect' _ t') (list_term_rect _ rest)
          end) _ l) 
      end.
  End TermRect.

  Inductive bone :=
  | H : hook -> list typed_skel_var -> typed_term -> list typed_skel_var -> bone
  | F : forall (f : filter), list typed_skel_var -> list typed_skel_var -> bone
  | B : list (list bone) -> list typed_skel_var -> bone.
  Definition skeleton := list (bone).

  Section BoneAll.
    Variable (P: bone -> Type).
    Variable (Q: skeleton -> Type).
    Inductive BAll : skeleton -> Type :=
    | b_nil_all: BAll []
    | b_cons_all: forall {b bl}, P b -> BAll bl -> BAll (b::bl).
    Inductive SkAll : list skeleton -> Type :=
    | sk_nil_all: SkAll []
    | sk_cons_all: forall {s sl}, Q s -> SkAll sl -> SkAll (s::sl).
  End BoneAll.

  Section BoneRect.
    Variable (P: bone -> Type).
    Variable filters: forall f xi xo, P (F f xi xo).
    Variable hooks: forall h xs t xo, P (H h xs t xo).
    Variable branchings: forall l V,
      SkAll (BAll P) l -> P (B l V).

    Fixpoint bone_rect' (b:bone): P b :=
      match b with
      | F f xi xo => filters f xi xo
      | H h xs t xo => hooks h xs t xo
      | B Ss V => branchings Ss V ((fix list_skeleton_rect' Ss :SkAll (BAll P) Ss :=
          match Ss return SkAll (BAll P) Ss with
          | [] => sk_nil_all (BAll P)
          | s :: s_rest => sk_cons_all (BAll P) ((fix skeleton_rect' s : (BAll P) s :=
          match s return (BAll P) s with
          | [] => b_nil_all P
          | b :: b_rest => b_cons_all P (bone_rect' b) (skeleton_rect' b_rest)
          end) s) (list_skeleton_rect' s_rest) end) Ss) 
      end.
  End BoneRect.

  Definition skeletal_semantics :=
    list (hook * list typed_skel_var * constructor * list typed_skel_var * skeleton * list typed_skel_var).

  Notation "a 'and' b" := (if a then b else false) (right associativity, at level 39).

  Definition sort_of (x:typed_skel_var) : sort :=
    projT1 x.

  Definition sort_of_term (x:typed_term) : term_sort :=
    projT1 x.

  Fixpoint find (e: list typed_skel_var) (xo:string): option sort :=
    match e with
    | [] => None
    | existT _ s1 (skel_var_intro s'1 n1) :: e' =>
        if (string_dec n1 xo) then Some s1 else find e' xo
    end.
Fixpoint unfold_list_option {A:Type} (l:list (option A)): option (list A):=
    match l with
    | [] => Some []
    | None :: _ => None
    | Some x :: l' =>
        match unfold_list_option l' with
        | None => None
        | Some l'' => Some (x :: l'')
        end
    end.

  Fixpoint skel_name (s:typed_skel_var): string :=
    match s with
    | existT _ _ (skel_var_intro _ n) => n
    end.

  Fixpoint well_formed_env (e: list typed_skel_var): bool :=
    match e with
    | [] => true
    | existT _ s1 (skel_var_intro s'1 n1) :: e' =>
        match find e' n1 with
        | None => well_formed_env e'
        | Some _ => false
        end
    end.

  Fixpoint all_true {sl: list term_sort} {l:list_term sl} (al:All (fun _ _ => bool) l):=
    match al with
    | nil_all _ => true
    | cons_all _ b rest => b and all_true rest
    end.

  Definition well_formed_term (e: list typed_skel_var) (l:typed_term): bool :=
    match l with
    | existT _ s t => fun sv constr => term_rect' (fun _ _ => bool) sv constr s t
    end
    (fun s v => match v with skel_var_intro _ n => match find e n with
                        | Some s' => if sort_eq_dec (Term s) s' then true else false
                        | None => false
                        end end)
    (fun c l al => all_true al).

  Fixpoint follow (e: list typed_skel_var) (s: skeleton) (wfs:BAll (fun _ => list typed_skel_var -> option (list typed_skel_var)) s) :option (list typed_skel_var) :=
    match s return BAll (fun _ => list typed_skel_var -> option (list typed_skel_var)) s -> option (list typed_skel_var) with
    | [] => fun _ => Some e
    | b :: rest => fun wfs =>
        match wfs in BAll _ (s::l) return option(list typed_skel_var) with
        | b_cons_all _ b1 b2 =>
            match b1 e with
            | None => None
            | Some e' => follow e' rest
        (match wfs in BAll _ (s::l) return BAll _ l with
        | b_cons_all _ b1 b2 => b2 end)
            end
        end
    end wfs.

  Fixpoint unfold (e:list typed_skel_var) (Ss:list skeleton) (wfs:SkAll (BAll (fun _ => list typed_skel_var -> option (list typed_skel_var))) Ss) :list (option (list typed_skel_var)) :=
    match Ss return SkAll (BAll (fun _ => list typed_skel_var -> option (list typed_skel_var))) Ss -> list (option (list typed_skel_var)) with
    | [] => fun _ => []
    | s :: rest =>
        fun wf =>
        follow e s match wf with sk_cons_all _ e1 e2 => e1 end ::
         unfold e rest
        match wf in SkAll _ (s::l) return SkAll _ l with
        | sk_cons_all _ e1 e2 => e2
        end
    end wfs.

  Definition option_eq_dec {A} (type_eq_dec: forall (a1 a2:A), {a1 = a2} + {a1 <> a2}) :
    forall (a1 a2:option A), {a1 = a2} + {a1 <> a2}.
  Proof.
    intros a1 a2; destruct a1 as [a1|], a2 as [a2|];
        try (right; congruence); [|left;congruence].
    destruct(type_eq_dec a1 a2); [left|right]; congruence.
  Defined.

  Definition option_sort_eq_dec := option_eq_dec sort_eq_dec.

  Definition well_formed_bone (e:list typed_skel_var) (b:bone): option (list typed_skel_var):=
  (fun f h b =>
    bone_rect' (fun _ => list typed_skel_var -> option (list typed_skel_var)) f h b)
  (fun f xi xo e =>
        let '(fs1, fs2) := fsort f in
        let e' := app xo e in
        if unfold_list_option (map (find e) (map skel_name xi)) and
        list_sort_eq_dec (map sort_of xi) fs1 and
        list_sort_eq_dec (map sort_of xo) fs2 and
        (well_formed_env e') then Some e' else None)
  (fun h xs t xo e =>
        let '(hs1, hs2, hs3) := hsort h in
        let e' := app xo e in
        if unfold_list_option (map (find e) (map skel_name xs)) and
        list_sort_eq_dec (map sort_of xs) (map Flow hs1) and
        term_sort_eq_dec (sort_of_term t) (Prgm hs2) and
        well_formed_term e t and
        list_sort_eq_dec (map sort_of xo) (map Flow hs3) and
        (well_formed_env e') then Some e' else None)
  (fun Ss V wfs e =>
        let e' := app e V in
        if match unfold_list_option (unfold e Ss wfs) with
        | None => false
        | Some e' => forallb (fun e => forallb (fun v => match v with (existT _ t (skel_var_intro _ n)) => option_sort_eq_dec (find e n) (Some t) and true end) V) e'
        end and
        well_formed_env e'
        then Some e' else None) b e.


  Fixpoint well_formed_skeleton (e:list typed_skel_var) (s:skeleton): option (list typed_skel_var) :=
    match s with
    | [] => Some e
    | b :: rest =>
        match well_formed_bone e b with
        | Some e' => well_formed_skeleton e' rest
        | None => None
        end
    end.

  Fixpoint well_formed (s:skeletal_semantics) : bool :=
    match s with
    | [] => true
    | (h, xs, c, xt, sk, xo) :: s' =>
        let '(hs1, hs2, hs3) := hsort h in
        let '(cs1, cs2) := csort c in
        list_sort_eq_dec (map sort_of xt) (map Term cs1) and
        list_sort_eq_dec (map Flow hs1) (map sort_of xs) and
        prgm_sort_eq_dec hs2 cs2 and
        list_sort_eq_dec (map sort_of xo) (map Flow hs3) and
        match well_formed_skeleton (app xs xt) sk with
        | None => None
        | Some e => unfold_list_option (map (find e) (map skel_name xo))
        end and
        well_formed s'
    end.

  (*Definition well_formed_semantics :=
    {s:skeletal_semantics & well_formed s = None}.*)

End Skeleton.

Record language := mklang
  {  l_cons: Type;
     l_filter: Type;
     l_hook: Type;
     l_base_sort: Type;
     l_flow_sort: Type;
     l_prgm_sort: Type;
     l_base_sort_eq_dec: forall x y: l_base_sort, {x = y} + {x <> y};
     l_flow_sort_eq_dec: forall x y: l_flow_sort, {x = y} + {x <> y};
     l_prgm_sort_eq_dec: forall x y: l_prgm_sort, {x = y} + {x <> y};
     l_csort: l_cons ->
        ( list (term_sort l_base_sort l_prgm_sort) *
          l_prgm_sort);
     l_fsort: l_filter ->
        ( list (sort l_base_sort l_flow_sort l_prgm_sort) *
          list (sort l_base_sort l_flow_sort l_prgm_sort));
     l_hsort: l_hook ->
        ( list l_flow_sort * l_prgm_sort * list l_flow_sort );
     l_semantics: skeletal_semantics l_cons l_filter l_hook l_base_sort l_flow_sort l_prgm_sort l_csort}.

