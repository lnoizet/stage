# Fichiers de mon stage du 23 avril au 26 juillet

Dans le dossier rapport, mon rapport de stage.

Dans le dossier CompilCertif, mes travaux sur la compilation certifiée à l'aide
de sémantiques squelettiques. Deux sous-dossiers

## generator

Un générateur de code coq à partir d'un fichier de sémantique squelettique.

## ArithStack

Une preuve de principe avec la compilation d'un langage arithmétique vers un
langage à pile.
