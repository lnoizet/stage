\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usetheme{CambridgeUS}
\usepackage{amssymb}
\usepackage{bussproofs}
\usepackage{amsmath}



\author{Louis Noizet}
\title{Fair Reactive Programming}
\subtitle{report on article from A. Cave \it{et al.}}

\newcommand{\rl}[1]{\RightLabel{\scriptsize #1}}
\newcommand\arr\rightarrow
\newcommand\nxt\bigcirc
\newcommand\alw\square
\newcommand\eve\lozenge
\newcommand\unt{\mathcal U}
\DeclareMathOperator\inj{inj}
\DeclareMathOperator\iter{iter}
\DeclareMathOperator\coit{coit}
\DeclareMathOperator\out{out}
\DeclareMathOperator\inr{inr}
\DeclareMathOperator\inl{inl}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}
\begin{frame}
  \frametitle{Introduction}
  Temporal logic:

  \begin{itemize}
  \item Typed Lambda-calculus
  \pause\item ``next'' written $\nxt$, (with a constructor $\bullet$)
  \pause\item Least fixpoint $\mu$
  \item Greatest fixpoint $\mu$
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Useful modalities}

  Always modality:
  
  \noindent \textbf{codata} $\alw A$ = Cons of $A \times \nxt \alw A$

  \pause\vspace{1em}

  Eventually modality:
  
  \noindent \textbf{data} $\eve A$ = Now of $A$ \textbar{}
  Later of $\nxt \eve A$

  \pause\vspace{1em}
  
  Until modality:
  
  \noindent \textbf{data} $A \unt B$ =
  Continue of $A\times\nxt(A\unt B)$ \textbar{} Stop of $B$
\end{frame}

\begin{frame}
  \frametitle{Examples}
  \noindent\textbf{eval\_eventually}~: \mbox{$\alw A\arr\eve(A\arr B)\arr\eve B$}~:
  
  \noindent eval\_eventually aa ef = case ef of\\
  \textbar{} Now f $\Rightarrow$ Now f(hd aa)\\
  \textbar{} Later ef' $\Rightarrow$\\
  \indent let $\bullet$ ef' $\!\!$' = ef' in\\
  \indent let  $\bullet$ aa' = tl aa in\\
  \indent Later ($\bullet$ (eval\_eventually aa' ef' $\!\!$'))
\end{frame}
  

\begin{frame}
  \frametitle{Examples}
  \noindent\textbf{create\_until}~:
  \mbox{$\alw A\arr\eve B\arr A\unt B$}

  \noindent create\_until aa eb = case eb of\\
  \textbar{} Now b $\Rightarrow$ Stop b\\
  \textbar{} Later eb' $\Rightarrow$\\
  \indent let $\bullet$ eb' $\!\!$' = eb' in\\
  \indent let  $\bullet$ aa' = tl aa in\\
  \indent Continue (hd aa) ($\bullet$ (create\_until aa' eb' $\!\!$'))
\end{frame}

\begin{frame}
  \frametitle{Non-examples}
  We refuse~:
  
  
  \noindent\textbf{predictor}~: $\nxt A\arr A$\\
  predictor x = let $\bullet$ x' = x in x'
  \pause\vspace{1em}

  We also refuse case analysis:

  we don't have
  \mbox{$\nxt(A+B)\arr\nxt A+\nxt B$}. 
\end{frame}


\begin{frame}
  \frametitle{Temporal syntax}
  
  Sequents: ``$\Theta;\Gamma\vdash M:A$''

  \pause Usual inference rules on $\Gamma\vdash M:A$ with $\Theta$ left unchanged.
  
  \pause We add:
  
  \begin{minipage}{0.3\textwidth}
    \begin{prooftree}
      \AxiomC{$\cdot;\Theta\vdash M:A$}
      \UnaryInfC{$\Theta;\Gamma\vdash\bullet M:\nxt A$}
    \end{prooftree}
  \end{minipage}\hspace{2em}\begin{minipage}{0.65\textwidth}
    \begin{prooftree}
      \AxiomC{$\Theta;\Gamma\vdash M:\nxt A$}
      \AxiomC{$\Theta,x:A;\Gamma\vdash N:C$}
      \BinaryInfC{$\Theta;\Gamma\vdash
        \mbox{let }\bullet x=M\mbox{ in }N:C$}
    \end{prooftree}
  \end{minipage}

\end{frame}


\begin{frame}
  \frametitle{Fixpoint syntax}
  We also add constructors and destructors for the fixpoints~:


  \begin{prooftree}
    \AxiomC{$\Theta;\Gamma\vdash M:F[\mu X.F/X]$}
    \UnaryInfC{$\Theta;\Gamma\vdash\inj M:\mu X.F$}
  \end{prooftree}
  \begin{prooftree}
    \AxiomC{$\cdot;x:F[C/X]\vdash M:C$}
    \AxiomC{$\Theta;\Gamma\vdash N:\mu X.F$}
    \BinaryInfC{$\Theta;\Gamma\vdash\iter_{X.F}(x.M) N:C$}
  \end{prooftree}
  \begin{prooftree}
    \AxiomC{$\cdot;x:C\vdash M:F[C/X]$}
    \AxiomC{$\Theta;\Gamma\vdash N:C$}
    \BinaryInfC{$\Theta;\Gamma\vdash\coit_{X.F}(x.M) N:\nu X.F$}
  \end{prooftree}
  \begin{prooftree}
    \AxiomC{$\Theta;\Gamma\vdash M:\nu X.F$}
    \UnaryInfC{$\Theta;\Gamma\vdash\out M:F[\nu X.F/X]$}
  \end{prooftree}
\end{frame}

\begin{frame}
  \frametitle{Examples}
  This is the typing of $0:=\inj(\inr())$.

  \begin{prooftree}
    \AxiomC{$\Theta;\Gamma\vdash ():1$}
    \UnaryInfC{$\Theta;\Gamma\vdash\inr ():1+\mu X.(1+X)$}
    \UnaryInfC{$\Theta;\Gamma\vdash\inj(\inr()) M:\mu X.(1+X)$}
  \end{prooftree}
  

  \pause and here is the derivation for the constant unit 1-stream~:
  
  \begin{prooftree}
    \AxiomC{$\cdot;x:A\vdash ():1$}
    \AxiomC{$\cdot;x:A\vdash x:A$}
    \BinaryInfC{$\cdot;x:A\vdash ((),x):(1 \times X)$}
    \AxiomC{$\cdot;\cdot\vdash a:A$ (any $a$ of any type $A$)}
    \BinaryInfC{$\cdot;\cdot\vdash\coit_{X.1\times X}(x.((),x)) a:\nu
      X.(1\times X)$}
  \end{prooftree}
\end{frame}
  
\begin{frame}
  \frametitle{Reduction}

  Usual reduction rules plus one for $\bullet$, one for least fixpoint, one
  for greatest fixpoint.

  \pause $n$-contexts $\mathcal E_n$: context which goes through $n$
  $\nxt$-modalities $n\leq\omega$.
  
  \pause $s\rightsquigarrow_n t$ iff there is a $k$-context
  ($k\le n$) $\mathcal E_k$ s.t. $s=\mathcal E_k[s']$,
  $t=\mathcal E_k[t']$, and s.t. $s$ reduces to $t$.
  
  
  \pause We have strong normalization $\Rightarrow$ impossibility of
  $\bot$ := $\mu X.X$.
\end{frame}
\end{document}
