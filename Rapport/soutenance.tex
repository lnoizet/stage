\documentclass[french]{beamer}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{soulutf8}

\setlength{\marginparwidth}{2cm}
\usepackage{stmaryrd}
\usepackage{todonotes}
\usepackage{listings}
\usepackage{lstcoq}
\usepackage{bussproofs}
\usetheme{CambridgeUS}

\setbeamertemplate{navigation symbols}{}
\DeclareMathOperator{\Const}{Const}
\DeclareMathOperator{\Plus}{Plus}
\DeclareMathOperator{\Minus}{Minus}
\DeclareMathOperator{\Times}{Times}
\DeclareMathOperator{\Div}{Div}
\DeclareMathOperator{\Nop}{Nop}
\DeclareMathOperator{\Push}{Push}
\DeclareMathOperator{\Add}{Add}
\DeclareMathOperator{\Mul}{Mul}
\DeclareMathOperator{\Sub}{Sub}
\renewcommand{\Div}{\operatorname{Div}}
\DeclareMathOperator{\Eval}{Eval}



\title[Sémantiques squelettiques]{Compilation certifiée à l'aide de sémantiques squelettiques}
\subtitle{Encadré par Alan Schmitt}
\institute[Équipe Celtique]{Inria Rennes -- Bretagne Atlantique, Équipe Celtique}
\author{Louis Noizet}
\date{du 23 avril au 26 juillet 2019}

\input{macros_beamer}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}

\section{Introduction}
\begin{frame}[t]{Importance de la compilation certifiée}
  \vspace{2em}
  Cas d'un programme critique~:
  \only<1>{\vspace{2em}\includegraphics[width=\textwidth,page=1]{figures}}
  \only<2>{\vspace{2em}\includegraphics[width=\textwidth,page=2]{figures}}
  \only<3>{\vspace{2em}\includegraphics[width=\textwidth,page=3]{figures}}
  \only<4>{\vspace{2em}\includegraphics[width=\textwidth,page=4]{figures}}
  \only<5>{\vspace{2em}\includegraphics[width=\textwidth,page=5]{figures}}
  \only<6>{\vspace{2em}\includegraphics[width=\textwidth,page=6]{figures}}
\end{frame}

\begin{frame}{Compilateur correct}
  \begin{definition}
    Un compilateur \emph{correct} choisit \alert<1>{un} comportement parmi ceux admis par le langage source, et traduit ensuite ce comportement dans le langage cible exécutable
  \end{definition}
  \pause
  GCC n'est pas un compilateur correct\footnote<2->{Xuejun Yang \emph{et al.}, Finding and understanding bugs in C compilers}.
  
  CompCert en revanche est certifié.\pause

  Mais CompCert passe au total par 10 langages. C'est un fort coût en spécifications.
\end{frame}

\AtBeginSection{
  \begin{frame}
    \tableofcontents[currentsection]
\end{frame}}

\section{Sémantique squelettique}

\begin{frame}{Exemple}
  \only<-2>{\begin{figure}
    \begin{mathpar}
      \inferrule{
	\sigma, e \evalr \btrue \\
	\sigma, t_1 \evalr \ovar}
      {\sigma, \sif{e}{t_1}{t_2} \evalr \ovar}
      \and
      \inferrule{
	\sigma, e \evalr \bfalse \\
	\sigma, t_2 \evalr \ovar}
      {\sigma, \sif{e}{t_1}{t_2} \evalr \ovar}
    \end{mathpar}
    \caption{Règles habituelles pour la sémantique naturelle d'un \(\sif{}{}{}\)}
  \end{figure}}
  \only<3->{On décompose ce <<~squelette~>>~:}
  \uncover<2->{\begin{figure}
    \begin{multline*}
      \Rule{Eval}{\ivar,\sif{\tvar[1]}{\tvar[2]}{\tvar[3]}}
      {\\
      {\left[\alert<4>{\deriv{Eval}{\ivar{}}{\tvar[1]}{\fvar[1]}};
      \alert<5>{\branchesV[\mset{\ovar{}}]{\begin{aligned}
	&\alert<6>{\filter{\texttt{isTrue}}{\fvar[1]}{}};
	\deriv{Eval}{\ivar{}}{\tvar[2]}{\ovar{}}\\
	&\alert<6>{\filter{\texttt{isFalse}}{\fvar[1]}{}};
	\deriv{Eval}{\ivar{}}{\tvar[3]}{\ovar{}}
      \end{aligned}}}
      \right]}
    }{\alert<7>{\ovar{}}}
    \end{multline*}
    \caption{Squelette pour la structure \(\sif{}{}{}\)}
  \end{figure}}
  \only<4>{\emph{Crochet}~: on évalue récursivement le terme \(\tvar[1]\) dans l'état \(\ivar{}\) et on affecte le résultat à \(\fvar[1]\).}
  \only<5>{\emph{Branchement}~: On effectue parallèlement chacune des branches, et on met ensuite les résultats en commun. On ne retient que la valeur de \(\ovar{}\)}
  \only<6>{\emph{Filtre}~: On évalue si \(\fvar[1]\) est vrai ou s'il est faux. Le fonctionnement de ces primitives n'est pas spécifié.}
  \only<7>{Le résultat est le contenu de la variable \(\ovar{}\).}
\end{frame}


\begin{frame}{Formellement}
  \pause
\begin{align*}
  \textsc{Terme} \quad t & \Coloneqq
  b \mid x_i \mid c (t..t)
\end{align*}
\pause
\begin{align*}
  \textsc{Règle}\quad \textsc{Eval}(c(t..t)) & \Coloneqq S_{\myvecp{\svar{x}}{n}}
\end{align*}
\pause

Un squelette est de la forme \(S_{\myvecp{\svar{x}}{n}}\) où \(S\) est un corps de squelette.

\pause
\begin{align*}
  \textsc{Corps} \quad \skel & \Coloneqq \emptylist \mid \cons{B}{\skel}\\
  \textsc{Os} \quad  B & \Coloneqq
  \deriv{H}{\myvec{\svar{x}}{n}}{t}{\myvecp{\svar{y}}{m}} \mid
  \filter{\texttt{f}}{\myvec{\svar{x}}{n}}{\myvecp{\svar{y}}{m}}
      \mid \branches{\skel..\skel}
\end{align*}

\end{frame}


\begin{frame}{Interprétation concrète}
  \only<1>{Un squelette a plusieurs interprétations.
    
  Voyons le cas de l'interprétation concrète (sémantique naturelle).}
  
  \only<2-3>{On évalue le terme \(\sif{\tvar[1]}{\tvar[2]}{\tvar[3]}\) dans un certain état \(\ivar\).}

  \only<2-3>{\uncover<3>{
  On agit de manière inductive. On peut supposer par exemple que \(\operatorname{Eval}(\ivar,\tvar[1])=ff\) et \(\operatorname{Eval}(\ivar,\tvar[3])=42\).

    \begin{prooftree}
      \AxiomC{\vdots}
      \noLine\UnaryInfC{\(\operatorname{Eval}(\ivar,\tvar[1])=ff\)}
      \AxiomC{\vdots}
      \noLine\UnaryInfC{\(\operatorname{Eval}(\ivar,\tvar[3])=42\)}
      \BinaryInfC{\(\operatorname{Eval}(\ivar,\sif{\tvar[1]}{\tvar[2]}{\tvar[3]})=?\)}
\end{prooftree}}}
  
  \uncover<4->{L'idée est de faire évoluer un environnement E qui contiendra les calculs au fur et à mesure}
  \vspace{-2em}
  \only<3>{ }
  \only<4->{\begin{figure}
    \begin{multline*}
      \uncover<4->{E,}{\left[\only<-15>{\only<-5>{\alert<5>{\deriv{Eval}{\ivar{}}{\tvar[1]}{\fvar[1]}};}
      \alert<7,15>{\branchesV[\mset{\ovar{}}]{\begin{aligned}
	\only<-9>{&\only<8->{E_0,}{\only<-9>{\alert<9>{\filter{\texttt{isTrue}}{\fvar[1]}{}};}}
	\alert<13>{\deriv{Eval}{\ivar{}}{\tvar[2]}{\ovar{}}}}\\
	&\only<8-15>{E_1\only<-13>,}\only<-11>{\alert<11>{\filter{\texttt{isFalse}}{\fvar[1]}{}};}
	\only<-13>{\alert<13>{\deriv{Eval}{\ivar{}}{\tvar[3]}{\ovar{}}}}
      \end{aligned}}}
}\right]}_{\ovar{}}\only<17>{\rightarrow 42}
  \end{multline*}
    \caption{Squelette pour la structure \(\sif{}{}{}\)}
  \end{figure}}
  \uncover<3->{\(Q = \{(\textsc{Eval},\ivar,\tvar[1],ff),(\textsc{Eval},\ivar,\tvar[3],42)\}\)}

{\uncover<4->{\(E = \{\only<6->{\fvar[1]\mapsto ff\only<16->{,\ovar{}\mapsto42}}\}\)}}
  \only<8-9>{\(; E_0 = \{\only<6->{\fvar[1]\mapsto ff}\}\)}
  \uncover<8-15>{\(; E_1 = \{\only<6->{\fvar[1]\mapsto ff}\only<14->{, \ovar{}\mapsto42}\}\)}
\end{frame}

\section{Compilation certifiée en sémantique squelettique}

\begin{frame}{Principe}
  \pause
  Deux langages avec leurs sémantiques squelettiques

  \pause
  Un compilateur

  \pause
  On prouve que la sémantique est préservée.

  \pause
  On prend comme langages~:
  \pause
  \begin{itemize}
    \item Un langage arithmétique de base,
  \pause
    \item Un langage à pile.
  \end{itemize}
\end{frame}

\begin{frame}{Premier langage}
  \only<1>{On a les expressions arithmétiques suivantes~:
    \begin{align*}
      \exprS{}_A \quad e \Coloneqq
      & | \operatorname{Const} i\\
      & | \operatorname{Plus} e~e\\
      & | \operatorname{Minus} e~e\\
      & | \operatorname{Times} e~e\\
      & | \operatorname{Divide} e~e\\
  \end{align*}}

  \only<2-3>{On donne leur interprétation squelettique~:
    \begin{figure}
      \Rule{Eval}{\operatorname{Const}{\tvar}}
	{\left[\filter{\texttt{litToVal}}{\tvar}{\ovar}\right]}{\ovar}
	\\
	\alt<2>{\begin{multline*}
	\Rule{Eval}{\operatorname{Plus}\tvar[1]{}~\tvar[2]{}}
	{[
	    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};\\
	    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
	    \filter{\texttt{add}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
	]}{\ovar}
      \end{multline*}
	\\
	\begin{multline*}
	  \Rule{Eval}{\operatorname{Minus}\tvar[1]{}~\tvar[2]{}}
	{[
	    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};\\
	    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
	    \filter{\texttt{sub}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
	]}{\ovar}
    \end{multline*}}
	{\begin{multline*}
	    \Rule{Eval}{\operatorname{Times}\tvar[1]{}~\tvar[2]{}}
	{[
	    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};\\
	    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
	    \filter{\texttt{mul}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
	]}{\ovar}
      \end{multline*}
	\\
	\begin{multline*}
	  \Rule{Eval}{\operatorname{Divide}\tvar[1]{}~\tvar[2]{}}
	{[
	    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};\\
	    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
	    \filter{\texttt{div}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
	]}{\ovar}
    \end{multline*}}
    \end{figure}
  }
\end{frame}

\begin{frame}{Deuxième langage}
  \only<1>{On a les déclarations suivantes~:
    \begin{align*}
      \exprS{}_S \quad s \Coloneqq
      & | \operatorname{Nop}\\
      & | \operatorname{s;s}\\
      & | \operatorname{Push}i\\
      & | \operatorname{Add}\\
      & | \operatorname{Mul}\\
      & | \operatorname{Sub}\\
      & | \operatorname{Div}\\
  \end{align*}}

  \only<2-3>{On donne leur interprétation squelettique~:
    \begin{figure}
      \alt<2>{
	  \[\Rule{Eval}{\ivar{},\operatorname{Nop}}
	  {\left[\right]}{\ivar}\]
	\begin{multline*}
	  \Rule{Eval}{\ivar{},(\tvar[1]{} ; \tvar[2]{})}
	  {\left[
	      \deriv{Eval}{\ivar{}}{\tvar[1]{}}{\ivar'};
	      \deriv{Eval}{\ivar'}{\tvar[2]{}}{\ivar''}
	  \right]}{\ivar''}
	\end{multline*}
	  \[\Rule{Eval}{\ivar{},\operatorname{Push}~\tvar[1]}
	  {\left[
	      \begin{multlined}[][\arraycolsep]
		\filter{\texttt{litToVal}}{\tvar[1]}{\fvar[1]};
		\filter{\texttt{push}}{\fvar[1], \ivar}{\ovar}
	      \end{multlined}
	\right]}{\ovar}\]
	\begin{multline*}
	  \Rule{Eval}{\ivar{},\operatorname{Add}}
	  {[
	    \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')};
	    \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')};\\
	    \filter{\texttt{add}}{\fvar[1], \fvar[2]}{\fvar[3]};
	    \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
	  ]}{\ovar}
	\end{multline*}}{
	\begin{multline*}
	  \Rule{Eval}{\ivar{},\operatorname{Sub}}
	  {[
	    \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')};
	    \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')};\\
	    \filter{\texttt{sub}}{\fvar[1], \fvar[2]}{\fvar[3]};
	    \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
	  ]}{\ovar}
	\end{multline*}
	\begin{multline*}
	  \Rule{Eval}{\ivar{},\operatorname{Mul}}
	  {[
	    \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')};
	    \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')};\\
	    \filter{\texttt{mul}}{\fvar[1], \fvar[2]}{\fvar[3]};
	    \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
	  ]}{\ovar}
	\end{multline*}
	\begin{multline*}
	  \Rule{Eval}{\ivar{},\operatorname{Div}}
	  {[
	    \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')};
	    \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')};\\
	    \filter{\texttt{div}}{\fvar[1], \fvar[2]}{\fvar[3]};
	    \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
	  ]}{\ovar}
      \end{multline*}}
  \end{figure}}
\end{frame}

\begin{frame}{Compilateur}
  On définit le compilateur de la manière suivante~:
    \begin{align*}
      compile(e) \Coloneqq &match\text{ }e\text{ }with\\
      & | \operatorname{Const} i\Rightarrow\operatorname{Push}i\\
      & | \operatorname{Plus} e_1~e_2\Rightarrow compile(e_1);compile(e_2);\operatorname{Add}\\
      & | \operatorname{Minus} e_1~e_2\Rightarrow compile(e_1);compile(e_2);\operatorname{Sub}\\
      & | \operatorname{Times} e_1~e_2\Rightarrow compile(e_1);compile(e_2);\operatorname{Mul}\\
      & | \operatorname{Divide} e_1~e_2\Rightarrow compile(e_1);compile(e_2);\operatorname{Div}\\
  \end{align*}
\end{frame}

\begin{frame}{Théorème de correction}
\begin{theorem}[correction]
  \begin{gather*}
    \forall e\in \exprS_A, \forall s, s'\in \stackS,\\
    \operatorname{Eval_{stack}}(s, compile\;e)= s'
    \;\rightarrow\;\exists n, \left[(s' = n :: s)
    \wedge \operatorname{Eval_{arith}}(e) = n\right]
  \end{gather*} 
\end{theorem}
  \begin{proof}
    \begin{itemize}
      \pause\item On travaille par récurrence forte sur la hauteur de l'arbre de dérivation. Soit $n$ cette hauteur.
      \pause\item On fait une disjonction de cas sur $e$.
	\begin{itemize}
	  \pause\item Si \(e = \Const i\), alors \(compile(e)=\Push i\), donc on a bien le résultat.
	  \pause\item Si \(e = \Plus e_1~e_2\), alors \(compile(e)=compile(e_1);compile(e_2);\operatorname{Add}\). Donc on a un sous-arbre qui prouve que \(\operatorname{Eval_{stack}}(s, compile\;e_1)= s_1\) et un sous-arbre qui prouve que \(\operatorname{Eval_{stack}}(s_1, compile\;e_2)= s'\). On en déduit \(\exists n_1,n_2\), et on conclut.
	  \pause\item …
	\end{itemize}
    \end{itemize}
  \end{proof}
\end{frame}

\section{Formalisation en Coq}

\begin{frame}[t]
  Sémantique squelettique en Coq~:
  \pause
  \only<2->{\begin{itemize}
  \only<2->{\item {\color{orange}Un fichier Semantics.v}}
  \only<4->{\item{\color[rgb]{0,.7,0} Deux fichiers de sémantiques squelettiques (Arith.sk et Stack.sk)}}
  \only<6->{\item{\color{orange} Un fichier Concrete.v}}
  \only<8->{\item{\color{red} Un fichier Compile.v}}
  \end{itemize}}
  \only<3>{Donne les définitions de base. Par exemple~:
  \lstinputlisting[language=coq,firstline=21,lastline=33]{../CompilCertif/ArithStack/Semantics.v}}
  \only<5>{Un générateur de code Coq traduit les fichiers de sémantique squelettique, avec l'aide du fichier Semantics.v, en des fichiers Coq.}
  \only<7>{Définition de l'interprétation concrète. On pourrait donc théoriquement choisir une autre interprétation assez facilement, et il suffirait de changer ce fichier.}
  \only<9>{On y définit le compilateur, et on en prouve la correction.}
\end{frame}

\section{Travaux connexes}

\begin{frame}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      Compilation certifiée
      \begin{itemize}
	\item CompCert
	\item CakeML
	\item Double WP
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      Description de sémantique
      \begin{itemize}
	\item Ott
	\item Lem
	\item \(\mathbb K\)
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}

\section{Travaux futurs}

\begin{frame}
  \begin{itemize}[<+->]
    \item Sémantique à petit pas
    \item Caml \(\rightarrow\) Zinc
    \item Compilation générique
  \end{itemize}
\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
