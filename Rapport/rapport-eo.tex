\documentclass{article}
\usepackage[esperanto]{babel}
\usepackage{soulutf8}
\usepackage{dsfont}
\usepackage{listings}
\usepackage{lstcoq}
\usepackage{booktabs}
\usepackage{subcaption}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{stmaryrd}

\usepackage{todonotes}

\lstset{basicstyle=\small\ttfamily,breaklines=true}

\usepackage{amsthm}

\theoremstyle{plain}  % style par défaut
\newtheorem{thm}{Teoremo}[section]

\title{Certigita kompilado per ostaraj semantikoj\\ %
\small M2-provtempa raporto}
\author{Louis Noizet, sub la direktado de Alan Schmitt}
\date{de la 23 de Aprilo ĝis la 26 de Julio, 2019}

\input{macros}

\begin{document}

\maketitle
\tableofcontents

\newpage

\section*{Enkonduko}
\addcontentsline{toc}{section}{Enkonduko}

En kelkaj kazoj, por gravegaj situacioj, kiel medicina
informatiko, transportiloj aŭ banko, oni ŝatus esti certa
havi programon kiu perfekte plenumas sian funkcion. Tial, en la
kazo de la 14-a metroa linio en Parizo, formala kontrolo de la
programo estis farita por garantii kelkajn proprecojn pri
certecojn kaj sekurecojn.

Tamen, fontprogramo ne estas tuj rulebla sed devas unue esti
tradukita de kompililo. \emph{Korekta} kompililo elektas konduton
inter tiuj kiujn la fontprogramo akceptas, kaj poste priskribas
tiun konduton en ruleblan lingvon. Oni do devus ankaŭ certigi la
kompililon por iĝi certa, ke la fakta konduto de la programo ja
estas fidela al unu el la kondutoj akceptataj de la fontprogramo.

Nu, la normaj kompililoj, kiel GCC kelkfoje eraras sur malfacilaj
programoj (vd. \cite{csmith}). Grandega progreso ĉi-rilate estis
la kreo de CompCert, C kompililo certigita kun Coq. Sed por povi
pruvi kompililon, necesas ke oni povas paroli de la semantiko de
la lingvoj kiujn oni uzas. Kaj por grandaj kompililoj, precipe se
oni volas fari optimumigojn dum la kompilado, estas pluraj peraj
lingvoj. En la kazo de CompCert, tio implicas doni la semantikon
de dek lingvoj. Tiu laboro povas esti plifaciligata kaj
grande aŭtomatigita kun la uzado de la ostara semantiko.

La ostara semantiko, enkondukita en 2019\cite{sk} estas
metalingvo kiu ebligas priskribi la semantikon de programada
lingvo. Ĝi povas poste facile esti interpretata interalie en
konkreta semantiko aŭ en abstrakta semantiko. La principo de la
ostara semantiko estas specifi la ĝeneralan strukturon de la
taksado de termo (vico de operacioj, ebla rikuro aŭ elekto inter
pluraj ebloj) sen detali la bazaj operacioj.

Dum mia provtempo, mi laboris pri la uzado de la ostara semantiko
por helpi kun la certigado de kompililoj. Pli precize, se oni konsideras du lingvojn kies ostaran semantikon oni havas kaj, kompililon inter tiuj du lingvoj, oni volas pruvi ke la kompililo estas korekta. La
celo de mia provtempo estis determini ĉu la uzado de la ostara
semantiko por specifi kaj pruvi la korektecon de kompililo alportas
ion, ĉu faciligante la pruvon, ĉu igante grandan parton de ĝi
sufiĉe sendependa je la lingvo, por ke ĝi povu esti
faktorigita.

En la unua parto de tiu raporto, mi difinos la ostaran
semantikon kaj ĝian funkciadon.
En la dua parto, mi detalos miajn esplorojn.
En la tria parto, mi maldetale prezentos rilatajn verkojn.
En la kvara parto, mi donos eblajn estontajn esplorojn pri ĉi-tiu
temo.

\newpage
\section{Ostara semantiko}

La ostaraj semantikoj estis kreitaj por reprezenti reuzeble
kaj module la strukturon de la semantikoj.
La ideo venas de la konstato ke la strukturo de semantiko
diseriĝas en rikuroj, branĉigoj kaj laŭvicaj taksadoj.
Tial, la celo estas profiti de ĉi-tiuj similecoj por krei modulan
ilon kiu povus utili samtempe al ĉiaj malsamaj traktadoj.

\subsection{Ekzemplo}

\begin{figure}
\begin{mathpar}
  \inferrule{
    \sigma, e \evalr \btrue \\
    \sigma, t_1 \evalr \ovar}
  {\sigma, \sif{e}{t_1}{t_2} \evalr \ovar}
  \and
  \inferrule{
    \sigma, e \evalr \bfalse \\
    \sigma, t_2 \evalr \ovar}
  {\sigma, \sif{e}{t_1}{t_2} \evalr \ovar}
\end{mathpar}
\caption{Kutimaj reguloj por la konkreta semantiko de \(\sif{}{}{}\)}
\label{fig:if:concrete}
\end{figure}

\begin{figure}
 \begin{multline*}
   \Rule{Eval}{\ivar,\sif{\tvar[1]}{\tvar[2]}{\tvar[3]}}
   {\\
   {\left[\deriv{Eval}{\ivar{}}{\tvar[1]}{\fvar[1]};
   \branchesV[\mset{\ovar{}}]{\begin{aligned}
     &\filter{\texttt{isTrue}}{\fvar[1]}{};
     \deriv{Eval}{\ivar{}}{\tvar[2]}{\ovar{}}\\
     &\filter{\texttt{isFalse}}{\fvar[1]}{};
     \deriv{Eval}{\ivar{}}{\tvar[3]}{\ovar{}}
   \end{aligned}}
   \right]}
   }{\ovar{}}
 \end{multline*}
 \caption{Ostaro por la strukturo \(\sif{}{}{}\)}
 \label{fig:if:skel}
\end{figure}

La figuro \ref{fig:if:concrete} priskribas la kutiman interpreton
en natura semantiko de kondiĉa stirfluo.
En ostara semantiko, oni priskribas tiun-ĉi konduton en la maniero
priskribata en figuro \ref{fig:if:skel}.

Ni diserigu tiun ``ostaron'':
\begin{itemize}
\item
  \(\Rule{Eval}{\ivar,\sif{\tvar[1]}{\tvar[2]}{\tvar[3]}}
  {}{\dots}\):
  Oni donas la difinon de la taksado de la esprimo
  \(\sif{\tvar[1]}{\tvar[2]}{\tvar[3]}\) en la stato \(\ivar\)
  kun la taksada funkcio \textsc{Eval}.
\item
\(\deriv{Eval}{\ivar{}}{\tvar[1]}{\fvar[1]}\): 
Oni rikure alvokas la taksadon de \(\tvar[1]\) en la stato
\(\ivar{}\), kaj oni valorizas \(\fvar[1]\) kun la rezulto.
Oni uzas la taksadan funkcion \textsc{Eval}, kiun oni nomas
  \emph{hoko}.
\item
  \[\branchesV[\mset{\ovar{}}]{\begin{aligned}
    &\filter{\texttt{isTrue}}{\fvar[1]}{};
    \deriv{Eval}{\ivar{}}{\tvar[2]}{\ovar{}}\\
    &\filter{\texttt{isFalse}}{\fvar[1]}{};
    \deriv{Eval}{\ivar{}}{\tvar[3]}{\ovar{}}
  \end{aligned}}~:\]
  Oni nomas tion \emph{branĉosto}.
  Oni taksas ĉiun linion aparte, kaj oni valorizas la variablon
  \(\ovar{}\) kun la rezulto de la taksado (la maniero kalkuli
  la entutan rezulton el la rezulto de ĉiu branĉo dependas je
  la elektita interpretado, vd. \ref{sssec:interp}).
\item
  \(\filter{\texttt{isTrue}}{\fvar[1]}{}\):
  Oni kalkulas ĉu \(\fvar[1]\) veras.
  La primitivojn kiel \(\texttt{isTrue}\) kaj
  \(\texttt{isFalse}\) oni nomas \emph{filtrostoj}.
  Ilia konduto ne estas specifita. Ĉiu filtrosto prenas kiel
  argumentojn kaj redonas iun (finian) nombron da valoroj,
  inkluzive neniun, kiel tie. En tiu kazo, la
  filtrosto agas kiel apartigilo, la branĉoj estantaj
  konservataj aŭ forĵetataj depende je la argumentoj donitaj al
  la filtrosto.
\item
  \(\deriv{Eval}{\ivar{}}{\tvar[2]}{\ovar{}}\): kiel supre.
\end{itemize}

Oni poste povas doni plurajn eblajn interpretojn al la ostaro
depende je la semantiko, kiun oni volas havi. Ekzemple, la
konkreta interpreto aŭ la abstrakta interpreto.
Oni ankaŭ povas uzi ĝin por eligi interpretilojn
(necro\footnote{\url{https://gitlab.inria.fr/skeletons/necro/}}),
statikajn analizilojn, \dots

\subsection{Formala difino}
\label{sssec:form}

Oni fiksas aron da bazaj termoj por la bazaj eroj de nia lingvo
(ekzemple la ĉenoj).

\begin{align*}
\textsc{Termo} \quad t & \Coloneqq
b \mid x_i \mid c (t..t)
\end{align*}
Termo estas aŭ baza termo, \(b\), aŭ variablo \(x_i\), aŭ
konstruilo aplikata al termoj.

Oni ankaŭ fiksas tipojn por klasifiki tiujn-ĉi termoj.
Oni distingas la programtipojn, kiuj estas la tipoj de la
termoj kiuj estos semantike taksataj (la termoj kun kapaj
konstruilo) kaj la baztipojn, kiuj estas la tipoj de la bazaj
termoj de la programo.
Oni ankaŭ fiksas flutipojn kiuj utilos por klasifiki la
valorojn kiujn oni uzos por la kalkulo (ekzemple stako).

Ostaro havas la formon \(\skel_{[\myvec{\svar{x}}{n}]}\), kie
\(\skel\) estas ostara enaĵo.
\begin{align*}
\textsc{Enaĵo} \quad \skel & \Coloneqq \emptylist \mid \cons{B}{\skel}\\
\textsc{Osto} \quad  B & \Coloneqq
\deriv{h}{\myvec{\svar{x}}{n}}{t}{\myvecp{\svar{y}}{m}} \mid
\filter{F}{\myvec{\svar{x}}{n}}{\myvecp{\svar{y}}{m}}
    \mid \branches{\skel..\skel}
\end{align*}

Ostara korpo estas vico de ostoj. Ĉiu osto estas hokosto, filtrosto aŭ branĉosto. Hokosto
\(\deriv{h}{\myvec{\svar{x}}{n}}{t}{\myvecp{\svar{y}}{m}}\)
reprezentas la rikuran taksadon de la semantiko de la termo
\(t\) en la stato \(\myvecp{\svar{x}}{n}\), kaj la valorizon de
ĝia rezulto al la variabloj \(\myvecp{\svar{y}}{m}\).
Filtrosto \(\filter{f}{\myvec{\svar{x}}{n}}
{\myvecp{\svar{y}}{n}}\) kontrolas ĉu la \(x_i\) kongruas kun
kelkaj antaŭkondiĉoj kaj, tiukaze, redonas valorojn por valorizi
la \(y_i\). Branĉigo estas el la formo
\(\branches{\myvec{\skel}{n}}\) kie \(V\) estas aro de variabloj
kiuj estas difinataj en ĉiuj branĉoj kaj uzeblaj post la taksado
de la branĉigo.

Por ĉiu aro \(E\), oni notas \(E^*\) la aron de la finiaj vicoj de
elementoj de \(E\). Formale, \(E^* \coloneqq \cup_{n\in\mathds
N}E^n\).
Doni la ostaran semantikon de lingvo signifas doni:
\begin{itemize}
\item la aron \(\Sigma_f\) de la flutipoj, la aron
  \(\Sigma_b\) de la baztipoj kaj la aron \(\Sigma_p\) de
  la programtipoj (oni metas
  \(\Sigma=\Sigma_f \cup \Sigma_b \cup \Sigma_p\), kaj oni
  distingas la termtipojn \(\Sigma_t =
  \Sigma_b\cup\Sigma_p\));
\item la aron \(C\) de la konstruiloj, kaj funkcion
  \(csort : C \rightarrow \Sigma_t^* \times \Sigma_p\) kiu
  donas la atendatajn tipojn kaj la redonan tipon (oni notas
  \(csort_1\) kaj \(csort_2\) ĝiajn du projekciojn);
\item aron \(F\) de filtrostoj, kaj funkcion
  \(fsort : F \rightarrow \Sigma^* \times \Sigma^*\), kiu donas
  la atendatajn tipojn kaj la redonajn tipojn;
\item aron \(H\) de hokoj;
\item por ĉiu hoko \(\textsc{h}\in H\), aron de tiaj reguloj:
  \(\Rule{h}{{\myvec{\svar{y}}{n},c\myvecp{\tvar}{n}}} {S}{}\)
  kie \(c\myvecp{\tvar}{n}\) estas la taksata termo (\(c\in
  C\)), la \(\myvec{\svar{y}}{n}\) donas la staton en kiu oni
  taksas la termon \(c\myvecp{\tvar}{n}\), kaj \(S\) estas
  ostaro.
\end{itemize}

La sintakso kiel ĝi estas ĉi-supre priskribita ne ekzakte
kongruas kun tiu difinita en la artikolo \cite{sk}, ĉar, la
ostara semantiko estanta tre freŝdata nocio, sia sintakso daŭre
estas evoluanta\footnote{Aldone, la sintakso tie prezentita estas tiu, kiu estis
uzita dum mia provtempo, sed ĝi poste ricevis kelkajn pliajn ŝanĝojn.}.
Ekzemple, oni nun uzas plurajn hokojn kvankam la
artikolo nur difinis unu. Danke al tio, oni povas taksi esprimon
malsammaniere laŭ la kunteksto. Krome, la komenca sintakso nur
ebligis havi unu variablon ene kaj ele de hokosto, dum oni nun akceptas havi ajnan nombron. Tio evitas devi ĉiufoje konstrui kaj detrui opojn.

\subsection{Konkreta interpretado}
\label{sssec:interp}

Oni poste difinas la konkretan interpretadon de tiu-ĉi semantiko.
Tiu estas natura grandpaŝa semantiko. Oni do difinas la
semantikon de termo per derivarboj kiuj reprezentas la rikurajn
alvokojn. La ideo do estas komenci per la arboj sen rikuro (sen
hokosto), kiuj estas la plej facilaj. Poste, oni grade etendas,
aldonante la interpretojn kies derivarboj estas pli kaj pli
grandaj. Por tio, oni difinas la konkretan interpreton kiel la
plej malgrandan fiksan punkton de bildigo \(\cinterpf{}\) kiu
bildigas aron de jam pruvitaj interpretoj (kiel kvaropoj (hoko,
stato, termo, rezulto)) al la derivarboj kiuj povas uzi tiujn
interpretojn kiel premisojn.

Formale, fermita termo estas termo en kiu enestas neniu
variablo. Ĉiun flutipon \(s\) oni bildigas al konkreta aro
de fluaj valoroj \(V_s\).
Oni notos \(V_{(\myvec{\svar{s}}{n})}\coloneqq
V_{s_1}\times\dots\times V_{s_n}\). Valoro estas aŭ flua valoro,
t.e elemento de unu el la \(V_s\), aŭ fermita termo.

Por ĉiu filtrosto \(f\) tia, ke \(fsort(f)=(s,t)\) kie \(s\) kaj
\(t\) estas listoj de tipoj, oni difinas ĝian interpreton
\(\llbracket f\rrbracket^I\in V_s\times V_t\). Oni notas
\(\filterinterp{f}{v}{w}\) por diri ke \((v,w)\in\llbracket
f\rrbracket^I\).
Por interpreti ostaron, oni laŭvice taksas ĝiajn ostarojn,
samtempe ĝisdatigante medion \(E\) kiu enhavas la jam kalkulitajn
valorojn --- \(E\) do estas parta bildigo inter la variabloj kaj la
valoroj --- kaj oni ankaŭ bezonas aron \(Q\) kiu enhavas la aron
de la rezultoj de la jam pruvitaj derivarboj, en la formo de
konkretaj kvaropoj \((\textsc h,v,t,w)\) kie \(\textsc h\in H\),
\(t\) estas fermita termo kaj \(v, w\) estas listoj de valoroj.

Ĉiun oston \(B\), oni bildigas al rilato \(\llbracket
B\rrbracket^I\) kiu bildigas la duopon de medio kaj aro \(Q\) de
konkretaj kvaropoj al la ĝisdatigita medio kaj al la sama aro
\(Q\). Ĉiun ostaron \(S\) oni bildigas al rilato \(\llbracket
S\rrbracket^I\) kiu ligas duopon kiel ĉi-supre kun elira
valoro.
Tiuj interpretoj estas difinitaj tiaj ke:
  \[\filterinterp{\filter{f}{\myvec{\svar{x}}{m}}
  {(\myvec{\svar{y}}{n})}}{E,Q}{(E' =
  \extsigvec{E}{\svar{y}}{\cval} ,Q)}\]
  kiam
  \(\filterinterp{f}{E(x_1)..E(x_m)}{(\myvec{\cval}{n})}\);
 \[\filterinterp{\deriv{h}{\myvec{\svar{x}}{m}}
 {t}{(\myvec{\svar{y}}{n})}}{E,Q}{(E' =
 \extsigvec{E}{\svar{y}}{\cval},Q)}\]
 kiam \((\textsc{h},(E(x_1)..E(x_m)),E(t),(\myvec{\cval}{n}))\in
  Q\);
 \[\filterinterp
 {\branchesV[\{\myvec{\svar{x}}{n}\}]{S_1..S_m}} {E,Q}{(E'  =
 \extsigvec{E}{\svar{x}}{\cval},Q)}\]
 kiam ekzistas \(i\) tia ke
  \(\filterinterp{[S_i]_{(\myvec{\svar{x}}{m})}}{E,Q}
  {(\myvec{\cval}{n})}\);
 \[\filterinterp{[]_{(\myvec{\svar{x}}{n})}}{E,Q}{(\myvec{\cval}{n})}\]
  kiam \(\forall i, E(x_i)=v_i\);
 \[\filterinterp{[B_1..B_n]_{(\myvec{\svar{x}}{m})}}
 {E,Q}{\interpOut}\] kiam
  \(\filterinterp{B_1}{E,Q}{(E',Q')}\) kaj
  \(\filterinterp{[B_2..B_n]_{(\myvec{\svar{x}}{m})}}
  {E',Q'}{\interpOut}\).

Tiam, oni difinas la bildigon de tuja konsekvenco \(\cinterpf{}\) sekve:
\begin{equation*}
\cinterpf{\quadset} =
\mset[(\textsc{h},\myvecp{\cstate}{n},\term,\myvecp{\cval}{m})]{
  \begin{gathered}
    \term = c\myvecp{\term}{n} \land
    csort_2(c) = \sort\\
    \Rule{h}{\myvec{\ivar}{n},c\myvecp{\tvar}{n}}{\skel}{[y_1..y_m]}\\
    \myvecp{\cstate}{n} : csort_1(c)\\
    \concrenv = \extsigvec{\extsigvec{}{\ivar}{\cstate}[n]}{\tvar}{\term}[n]\\
    \concrinterp{\skel}[\concrenv']\\
    \forall i =1..m,\:\concrenv'\funu{y_i} = \cval[i]
  \end{gathered}
}.
\end{equation*}

Tiel, \(\cinterpf[n]{\emptyset}\) enhavas la aron de la taksadoj
kiujn oni povas pruvi kun derivarbo kies grandeco estas maksimume
\(n\).
La konkreta interpreto \({\csem}\) estas tiam difinita per
\(\csem{} = \bigcup_n \cinterpf[n]{\emptyset}\). 

Ni konsideru la ekzemplon de la \(\sif{}\). Oni rememoriĝas ke
oni havas la ostaron de la \(\sif{}\) en figuro
\ref{fig:if:skel}. Oni volas kalkuli la semantikon de
$\textsc{Eval}({\cstate}, {\sif{\term[1]}{\term[2]}{\term[3]}})$.
Oni metas \(E=\extsigvec{\extsig{}{\ivar}{\cstate}}{\tvar}
{\term}[3]\), kaj oni supozas ke \(Q\) jam enhavas
\((\textsc{Eval},\ivar,\term[1], false)\) kaj
\((\textsc{Eval},\ivar,\term[3],\cval)\).
Tiam oni havas:
\[\filterinterp{\deriv{Eval}{\ivar{}}{\tvar[1]}{\fvar[1]}}{E,Q} {(E' =
\extsig{E}{\fvar[1]}{false},Q)}.\]
Oni eniras branĉigon do oni aparte traktas la du branĉojn.
\texttt{isTrue} ĵetas \emph{false} kaj \texttt{isFalse} konservas
ĝin: Oni havas
\[\filterinterp{\filter{\texttt{isFalse}}{false}{}}{E',Q}{(E',Q)}.\]
Poste, oni havas
\[\filterinterp{\deriv{Eval}{\ivar{}}{\tvar[3]}{\ovar{}}}{E',Q}{(E'' =
\extsig{E'}{\ovar{}}{v},Q)}.\] 
Tiam oni deduktas el tio:
\[\filterinterp{\branchesV[\mset{\ovar{}}]{\begin{aligned}
&\filter{\texttt{isTrue}}{\fvar[1]}{};
\deriv{Eval}{\ivar{}}{\tvar[2]}{\ovar{}}\\
&\filter{\texttt{isFalse}}{\fvar[1]}{};
\deriv{Eval}{\ivar{}}{\tvar[3]}{\ovar{}}
\end{aligned}}}{E',Q}{(E'' = \extsig{E'}{\ovar{}}{v},Q)}.\]
Tial, oni povas konkludi ke \((\textsc{Eval},\cstate,
{\sif{\term[1]}{\term[2]}{\term[3]}},\cval)\in\cinterpf{Q}\).

\section{Certigita kompilado kun la helpo de la ostara semantiko}

Oni volas uzi la ostaran semantikon por certigi kompililon. 
Ties uzado ja ebligas aŭtomatigi la difinon de la semantikoj de
la uzataj lingvojn, kiu povas esti peniga laboro.
Mi do kreis ilon ebliganta traduki la ostaran semantikon en Coq
lingvo. Oni donas tie ekzemplon de kompililoj kaj ideon de la
strukturo de ĝia pruvo.

\subsection{Generado de Coq kodo}

Dum mia provtempo, mi laboris pri la skribado de ilo por aŭtomate
generi na-Coq kodon el dosierojn donantajn la ostaran semantikon
de la lingvo (la sintakso de tiuj dosieroj estis difinitaj por
necro).

En la Coq programaro algluata al la artikolo kiu prezentas la
ostarajn semantikojn, la konkreta interpreto de lingvo jam estis
priskribita.
Tamen, estis pluraj baroj kontraŭ reuzi tiun kodon. Unue, ĝi
kongruas kun la sintakso de la artikolo, kiu ne plu tute estas
la uzata semantiko. Kaj cetere, kiel subporto de artikolo, ĝi
estas ekstreme formala, kiu ne faciligas ties uzadon.
Fine, ĝi ne estas skribita cele al kunvivigi plurajn semantikojn,
kiel estas la kazo kiam oni volas skribi pruvon de kompililo.
Mi do elektis reskribi la kodon de nulo, min inspiranta de la Coq
programaro algluita al la artikolo.

La kompleta kodo -- kiu ĉeestas tiun adreson:
\url{https://gitlab.inria.fr/lnoizet/stage/} -- estas dividita
tiel: estas dosiero por ĉiu lingvo, aŭtomate generata, kiu donas
la ostaran semantikon de tiu lingvo kaj generas rikordon
enhavanta ĉiujn datumojn de la lingvo. Ĝi sekvas tre precize la
formala difino donita en \ref{sssec:form}.
Mi reuzis por mia generilo la sintaksan analizilon, kaj la tipilon
de necro. Oni ankaŭ havas dosieron Semantics.v kiu konstruas la
induktajn difinojn. Oni ekzemple donas la difinon de termo kaj
osto:

\begin{lstlisting}[language=coq,escapeinside={|*}{*|}]
Inductive term :=
| term_sv : skel_var -> term
| term_constructor : constructor -> list term -> term
| term_base : base_sort -> nat -> term.                                                            
Inductive bone :=
| H : hook -> list skel_var -> term -> list skel_var -> bone
| F : forall (f : filter), list skel_var -> list skel_var -> bone
| B : list (list bone) -> list skel_var -> bone.
\end{lstlisting}

Fine, lasta dosiero nomata Concrete.v difinas la konkretan
interpreton de lingvo. La plej grava parto de tiu dosiero estas
la venonta difino:

\begin{lstlisting}[language=coq,escapeinside={|*}{*|}]
Inductive interpC : (skeleton _ _ _ _) -> Cstate -> Cresult -> Prop :=
| i_Cons : forall B S s1 s2 r,
  interpBC B s1 s2 ->
  interpC S s2 r ->
  interpC (B::S) s1 r
| i_Void : forall s t, interpC nil (s,t) s
with
interpBC : (bone _ _ _ _) -> Cstate -> Cstate -> Prop :=
| i_F : forall f (f1 : env) tl lv (l1 : list skel_var) l2,
  interpFC_opt f (List.map (find f1) l1) lv ->
  interpBC (F _ _ _ _ f l1 l2) (f1,tl) (add_asn f1 (l2) lv,tl)
| i_H : forall (h: (l_hook l)) (e : env) (T:concrete_quadruple -> Prop)
  (xf1 xf2 : list skel_var) (t:term _ (l_base_sort l)) (v : list value),
  match unfold_list_option (map (find e) xf1), eval e t with
  | Some xf1', Some t' => T (h, xf1', t', v)
  | _, _ => False
  end -> interpBC (H _ _ _ _ h xf1 t xf2) (e,T) (add_asn e xf2 v: env,T)
| i_B : forall Ss Si V e T vals e',
  List.In Si Ss -> interpC Si (e,T) e' ->
  map (find e') V = map Some vals ->
  interpBC (B _ _ _ _ Ss V) (e,T) (add_asn e V vals,T).
\end{lstlisting}

Laŭdifine, oni ne konas la konkretan interpreton de la filtrostoj
kaj de la tipoj, do la sintaksa dosiero uzata por generi la
na-Coq kodon difinanta la semantikon de la lingvo ne sufiĉas. Oni
do devas aldoni aksiomojn por sciigi specifojn pri la semantiko.
Ekzemple, oni bezonas aksiomon diranta ke, se filtrosto
\texttt{add} interpretas ke la adicio de \(a\) kaj \(b\) egalas
\(c\), tiam \(a\), \(b\) kaj \(c\) estas efektive de la atendataj
tipoj (\valS).
Ĉi-momente, la aksiomoj estas aldonataj rekte en Coq en la
dosiero Prelim.v sed oni povas imagi ke ili estu aŭtomate
generataj el plia dosiero kiu donus la specifojn.

\subsection{Difino de la lingvoj kaj iliaj ostaraj semantikoj}

Oni havas kiel unua lingvo etan aritmetikan lingvon kies esprimoj
estas difinataj tiel:

\begin{lstlisting}[escapeinside={|*}{*|}]
|*\(\exprS_A\)*| ::= 
| Const <\litS>
| Plus <|*\(\exprS_A\)*|> <|*\(\exprS_A\)*|> 
| Minus <|*\(\exprS_A\)*|> <|*\(\exprS_A\)*|>
| Times <|*\(\exprS_A\)*|> <|*\(\exprS_A\)*|>
| Divide <|*\(\exprS_A\)*|> <|*\(\exprS_A\)*|>
\end{lstlisting}

La semantiko de la lingvo estas tiu kiun oni imagas.
Oni donas ĝian ostaran semantikon.
La konstruiloj kaj filtrostoj estas donataj en fig.
\ref{fig:arith:sorts} kaj la ostara semantiko estas donata en
fig. \ref{fig:arith:semantics}.

\begin{figure}
\centering
\begin{equation*}
  \begin{array}[t]{|c|c|}
    \hline
    \text{\(c\)} & \text{Signaturo} \\
    \hline
    Const & \litS \rightarrow \exprS \\
    Plus & (\exprS \times \exprS) \rightarrow \exprS\\
    Minus & (\exprS \times \exprS) \rightarrow \exprS\\
    Times & (\exprS \times \exprS) \rightarrow \exprS\\
    Divide & (\exprS \times \exprS) \rightarrow \exprS\\
    \hline
  \end{array}
  \quad
  \begin{array}[t]{|c|c|}
    \hline
    f & \typsig{f}\\
    \hline
    \texttt{litToVal} & \litS \rightarrow \valT\\
    \texttt{add} & (\valT \times \valT) \rightarrow \valT\\
    \texttt{sub} & (\valT \times \valT) \rightarrow \valT\\
    \texttt{mul} & (\valT \times \valT) \rightarrow \valT\\
    \texttt{div} & (\valT \times \valT) \rightarrow \valT\\
    \hline
  \end{array}
\end{equation*}
\caption{Tipoj de la konstruiloj kaj filtrostoj de Arith}\label{fig:arith:sorts}
\end{figure}

\begin{figure}
\begin{align*}
  \Rulea{Eval}{Const~{\tvar}}
  {\left[\filter{\texttt{litToVal}}{\tvar}{\ovar}\right]}{\ovar}
  \\
  \Rulea{Eval}{Plus (\tvar[1]{}, \tvar[2]{})}
  {\left[
    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};
    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
    \filter{\texttt{add}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{Minus (\tvar[1]{}, \tvar[2]{})}
  {\left[
    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};
    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
    \filter{\texttt{sub}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{Times (\tvar[1]{}, \tvar[2]{})}
  {\left[
    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};
    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
    \filter{\texttt{mul}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{Divide (\tvar[1]{}, \tvar[2]{})}
  {\left[
    \deriv{Eval}{}{\tvar[1]{}}{\fvar[1]{}};
    \deriv{Eval}{}{\tvar[2]{}}{\fvar[2]{}};
    \filter{\texttt{div}}{\fvar[1]{}, \fvar[2]{}}{\ovar}
    \right]}{\ovar}
\end{align*}
\caption{Ostara semantiko de Arith}\label{fig:arith:semantics}
\end{figure}


La dua lingvo estas kutima staka lingvo.

\begin{lstlisting}[escapeinside={|*}{*|}]
|*\(\exprS_S\)*| ::=
| Nop | <|*\(\exprS_S\)*|> ; <|*\(\exprS_S\)*|>
| Push <literal>
| Add | Mul | Sub | Div
\end{lstlisting}

La esprimoj agas sur stako.
\lstinline{Nop} kaj \lstinline{;} faras kion oni imagas
\lstinline{Push} puŝas valoron sur la stakon, la operacioj
\lstinline{Add}, \lstinline{Sub}, \lstinline{Mul} kaj
\lstinline{Div} elstakigas la du unuajn nombrojn el la stakon, kaj
enstakigas la rezulton (la unua operando estas tiu kiu estas la
plej malsupre en la stako).
Oni difinas la konstruilojn kaj filtrostojn tiel (fig.
\ref{fig:stack:sorts}) kaj la ostaran semantikon tiel (fig.
\ref{fig:stack:semantics}).

\begin{figure}
\centering
\begin{equation*}
  \begin{array}[t]{|c|c|}
    \hline
    \text{\(c\)} & \text{Signaturo} \\
    \hline
    Nop & \unit \rightarrow \exprS \\
    ; & (\exprS \times \exprS) \rightarrow \exprS\\
    Push & \litS \rightarrow \exprS \\
    Add & \unit \rightarrow \exprS \\
    Sub & \unit \rightarrow \exprS \\
    Mul & \unit \rightarrow \exprS \\
    Div & \unit \rightarrow \exprS \\
    \hline
  \end{array}
  \quad
  \begin{array}[t]{|c|c|}
    \hline
    f & \typsig{f}\\
    \hline
    \texttt{litToVal} & \litS \rightarrow \valT\\
    \texttt{add} & (\valT \times \valS) \rightarrow \valT\\
    \texttt{sub} & (\valT \times \valS) \rightarrow \valT\\
    \texttt{mul} & (\valT \times \valS) \rightarrow \valT\\
    \texttt{div} & (\valT \times \valS) \rightarrow \valT\\
    \texttt{push} & (\valT \times \stackS) \rightarrow \stackS\\
    \texttt{pop} & \stackS \rightarrow (\valS \times \stackS)\\
    \hline
  \end{array}
\end{equation*}
\caption{Tipoj de la konstruiloj kaj filtrostoj de Stack}\label{fig:stack:sorts}
\end{figure}

\begin{figure}
\begin{align*}
  \Rulea{Eval}{\ivar{},Nop}
  {\left[\right]}{\ivar}
  \\
  \Rulea{Eval}{\ivar{},(\tvar[1]{} ; \tvar[2]{})}
  {\left[
    \deriv{Eval}{\ivar{}}{\tvar[1]{}}{\ivar'};
    \deriv{Eval}{\ivar'}{\tvar[2]{}}{\ivar''}
    \right]}{\ivar''}
  \\
  \Rulea{Eval}{\ivar{},Push~\tvar[1]}
  {\left[
    \begin{multlined}[][\arraycolsep]
      \filter{\texttt{litToVal}}{\tvar[1]}{\fvar[1]};
      \filter{\texttt{push}}{\tvar[1], \ivar}{\ovar}
    \end{multlined}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{\ivar{},Add}
  {\left[
    \begin{multlined}[][\arraycolsep]
      \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')} ;
      \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')} ;\\
      \filter{\texttt{add}}{\fvar[1], \fvar[2]}{\fvar[3]};
      \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
    \end{multlined}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{\ivar{},Sub}
  {\left[
    \begin{multlined}[][\arraycolsep]
      \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')} ;
      \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')} ;\\
      \filter{\texttt{sub}}{\fvar[1], \fvar[2]}{\fvar[3]};
      \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
    \end{multlined}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{\ivar{},Mul}
  {\left[
    \begin{multlined}[][\arraycolsep]
      \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')} ;
      \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')} ;\\
      \filter{\texttt{mul}}{\fvar[1], \fvar[2]}{\fvar[3]};
      \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
    \end{multlined}
    \right]}{\ovar}
  \\
  \Rulea{Eval}{\ivar{},Div}
  {\left[
    \begin{multlined}[][\arraycolsep]
      \filter{\texttt{pop}}{\ivar}{(\fvar[2]{},\ivar')} ;
      \filter{\texttt{pop}}{\ivar'}{(\fvar[1]{},\ivar'')} ;\\
      \filter{\texttt{div}}{\fvar[1], \fvar[2]}{\fvar[3]};
      \filter{\texttt{push}}{\fvar[3], \ivar''}{\ovar}
    \end{multlined}
    \right]}{\ovar}
\end{align*}
\caption{Ostara semantiko de Stack}\label{fig:stack:semantics}
\end{figure}

\subsection{Kompililo}

La kompililo estas tiel difinata (Coq sintakse):

\begin{lstlisting}[language=Coq, label={lst:compilo}, escapeinside={|*}{*|}]
Fixpoint compile (e:|*\(\exprS_A\)*|):|*\(\exprS_S\)*| :=
match e with
| Const i => Push i
| Plus e1 e2 => (compile e1) ; (compile e2) ; Add
| Minus e1 e2 => (compile e1) ; (compile e2) ; Sub
| Times e1 e2 => (compile e1) ; (compile e2) ; Mul
| Divide e1 e2 => (compile e1) ; (compile e2) ; Div
end
\end{lstlisting}

Oni volas pruvi la korektecon de tiu kompililo. Alidire, ĉiu ebla
konduto de la kompilata lingvo devas nepre esti akceptata konduto
por la fonta lingvo. En nia kazo, se oni havas aritmetikan
esprimon \(e\), kiam oni taksas la kompilaĵon de \(e\) sur iu ajn
stako, oni nepre ricevu la saman stakon, sur kies pinto oni havas
aldonita valoron \(n\) kiu estas valida interpreto de \(e\).

\begin{thm}[korekteco]
\begin{gather*}
  \forall e\in \exprS_A, \forall s, s'\in \stackS,\\
  (s, compile\;e, s')\in\csem_S
  \;\rightarrow\;\exists n,\left(\filterinterp{\texttt{push}}{n,s}{s'}
  \wedge ((), e, n)\in\csem_A\right)
\end{gather*} 
\end{thm}

Tie, ĉar la aritmetika lingvo estas simpla lingvo, oni havas unu
nuran eblan konduton, kaj se ekzistas tia \(n\), ĝi nepre estas
ununura. Kaj la kompililo ankaŭ estas kompleta, oni do povas
ankaŭ pruvi la teoremo de kompleteco, kiu diras ke se esprimo
\(e\) havas validan interpreton \(n\), tiam la kompilata versio
de \(e\) ankaŭ havas validan interpreton. Oni apriore supozas
nenion pri tiu interpreto, sed la korekteco diras, ke ĝi
kongruas kun valida konduto. Ĉar la aritmetika lingvo estas
determinista, oni eĉ povas aserti ke ĝi enstakigas \(n\).

\begin{thm}[kompleteco]
\begin{gather*}
  \forall e\in \exprS_A,\\
  ((), e, n)\in\csem_A \;\rightarrow\; \forall s\in\stackS, \exists s'\in\stackS,
  (s, compile\;e, s')\in\csem_S\\
\end{gather*} 
\end{thm}

Oni detalos en la venonta parto nur la pruvon de la korekteco,
sed la pruvo de la kompleteco estas ankaŭ donata en la Coq
programaro. Ĝia funkciado estas tre sama.

\subsection{Pruvo}

\subsubsection{Aksiomoj}

Kiel indikita ĉi-supre, oni bezonas aron de aksiomoj por povi
difini la konkretan interpreton de ostara semantiko.
Oni havas tri tipojn de aksiomoj:

\begin{itemize}
\item
  La bazaj kondiĉoj pri la interpreto de la filtrostoj:
  ekzemple, pri Arith, la filtrosto \texttt{litToVal} povas
  rilatigi nur elementon de tipo \litS{} kun elemento de tipo
  \(\valT\).
  \begin{lstlisting}[language=Coq, label={lst:lem:sort}, escapeinside={|*}{*|}]
  Lemma LitToValArithmeticSort:
  forall l1 l2,         
  AinterpFC Arithmetic.f_litToVal l1 l2 -> exists a b,
  l1 = [val_cterm ar (cterm_base ar A.s_literal a)] /\
  l2 = [val_flow ar A.s_value b].
  \end{lstlisting}
\item
  La enaj kondiĉoj de lingvo: ekzemple, oni ĉiam povu
  fari \texttt{push} sur ajna valoro kaj ajna stako, ankaŭ, se
  oni scias ke stako estas la rezulto de \texttt{push}, la uzo
  de \texttt{pop} devas ebligi retrovi la komencajn elementojn.
  Fine, unu lasta ekzemplo: la interpreto de \texttt{pop} estas
  injekta.
  \begin{lstlisting}[language=Coq, label={lst:lem:pushpop}, escapeinside={|*}{*|}]
  Axiom StackPushPop :
  forall l1 l2 l3,
  SinterpFC Stack.f_push l1 l2 ->
  SinterpFC Stack.f_pop l2 l3 -> l1 = l3.
  \end{lstlisting}
\item
  La kondiĉoj inter lingvoj: ekzemple, la tipoj \valS{} kaj
  \litS{} devas esti la ``samaj'' en la du lingvoj. Same pri la
    interpreto de la filtrostoj \texttt{add} kaj \texttt{litToVal} ekzemple. Oni
  eventuale povas havi kondiĉojn pli malsimplaj. En nia kazo,
  la rilato inter valoroj estas izomorfio, sed tiu ne nepre
  estas la kazo.
  \begin{lstlisting}[language=Coq, label={lst:lem:sameadd}, escapeinside={|*}{*|}]
  Axiom sameAdd :                                            
  forall a b c,                                        
  AinterpFC Arithmetic.f_add                         
  [val_flow ar A.s_value a;       
  val_flow ar A.s_value b]  
  [val_flow ar A.s_value c] <->                             
  SinterpFC Stack.f_add                 
  [val_flow st S.s_value (castValue a);
  val_flow st S.s_value (castValue b)]        
  [val_flow st S.s_value (castValue c)].
  \end{lstlisting}
\end{itemize}

Oni ne faras tie ĝisfundan liston de la necesaj aksiomoj, sed oni
supozas ke ĉiuj tipoj samnomaj en la du lingvoj estas fakte
izomorfia (kiu ebligas doni sencon al la formulo kiun oni volas
pruvi), kaj ke ĉiuj filtrostoj havanta la saman nomon en la du
lingvoj havas samajn interpretojn module tiujn izomorfiojn.

\subsubsection{Formala pruvo}

Por pruvi la kompililo, oni povas agi per struktura indukto sur
\(e\in \exprS_A\).
Tiam, oni evidente havas la rezulton por \(e = Const\;i\).
Poste, la heredan rezulton oni facile pruvas per la specifojn de
la reciprokaj agoj de \texttt{pop} kaj \texttt{push}.

Tiu pruvo funkcias tie ĉar la ostara semantiko estas tia ke ĉiu
alvoko de hoko faratas sur strikta subtermo. Ĝenerale, tiu ne
veras (ekzemple, se oni taksas iteracion, oni ofte faras agon kaj
retaksas la saman iteracion).
Tial, oni proponas formalan pruvon pli facile ĝeneraligebla, per
indukto sur la alteco de la derivarbo.

Formale, oni volas pruvi ke por ĉiuj \(e, s, s'\), oni ja havas:
$$(s, compile\;e, s')\in\csem_S
\rightarrow\exists n\in\valS, \filterinterp{\texttt{push}}{n,s}{s'}
\wedge ((), e, n)\in\csem_A$$
Alidire, por ĉiuj \(e, s, s'\), oni ja havas:
$$\left[\exists k,(s, compile\;e, s')\in\cinterpf[k]{\emptyset}\right]
\rightarrow\exists n\in\valS, \filterinterp{\texttt{push}}{n,s}{s'}
\wedge ((), e, n)\in\csem_A$$
Do sufiĉas pruvi ke:
$$\forall k,e,s,s',\;\left[(s, compile\;e, s')\in\cinterpf[k]{\emptyset}\right]
\rightarrow\exists n\in\valS, \filterinterp{\texttt{push}}{n,s}{s'}
\wedge ((), e, n)\in\csem_A$$
Oni pruvas tiun rezulton per forta indukto sur \(k\). Estu
\(k\in\mathds N\). Oni supozas ke la rezulto veras por ĉiu
\(j<k\). Estu \(e, s,s'\) tiaj ke \((s, compile\;e, s')\in\cinterpf[k]{\emptyset}\).
Tiam oni rezonas per kaza aŭo sur \(e\).
\begin{itemize}
\item Kiam \(e=Const\;i\), tiam \(compile\;e = Push\;i\) kaj do
  \(\filterinterp{\texttt{litToVal}}{i}{n}\). Tiam, oni ja
  havas la volatan rezulton.
\item Kiam \(e = Plus\;e_1\;e_2\), tiam
  \(compile\;e = (compile\;e_1)\;;\;(compile\;e_2)\;;\;Add\)
  kaj oni do havas
  \((\textsc{Eval}, s,compile\;e_1,s_1) \in\cinterpf[k-2]{\emptyset}\),
  \((\textsc{Eval}, s_1,compile\;e_2,s_2) \in\cinterpf[k-2]{\emptyset}\)
  kaj \((\textsc{Eval},s_2, Add, s') \in\cinterpf[k-1]{\emptyset}\).
  Per indukto, oni deduktas ke ekzistas \(n_1\) kaj \(n_2\)
  tiaj ke
  \(\filterinterp{\texttt{push}}{n_1,s}{s_1} \wedge ((), e_1, n_1)\in\csem_A\)
  kaj \(\filterinterp{\texttt{push}}{n_2,s_1}{s_2} \wedge ((), e_2, n_2)\in\csem_A\).
  Poste, ĉar \((\textsc{Eval},s_2, Add,
  s')\in\cinterpf[k-1]{\emptyset}\), oni deduktas ke ekzistas
  \(n\) tia ke \(\filterinterp{\texttt{add}}{n_1,n_2}{n}\) kaj
  \(\filterinterp{\texttt{push}}{n,s}{s'}\). Tial, oni ja havas
  \(((), e, n)\in\csem_A\).
\end{itemize}
La aliajn kazojn oni traktas simile.

\subsection{Renkontitaj malfacilaĵoj}

Ĉefe, mi dediĉis multe da tempo por elekti la manieron traduki
la semantikon en Coq. 
Mi devis trovi sintakson sufiĉe modula, evitanta la rediraĵoj kaj
facile legebla kaj uzebla. Kiel en ĉiu Coq kodo, oni volas ke la
specifoj estu difinita en sufiĉa simpla maniero por povi esti
konvinkata ke ili efektive kongruas kun kion oni volas pruvi. Oni
do esperas havi sufiĉe mallongan specifon. Entute, la pruvo de la
kompililo ĉi-supre detalata enhavas 889 liniojn da specifoj kaj
581 liniojn da pruvo.

Alia grava malfacilaĵo kiun mi renkontis estas la skribo de la
pruvo. Bona Coq pruvo devas esti komprenebla, devas eviti la
rediraĵojn, kaj devas esti facile adaptebla kiam oni ŝanĝas
elementojn en la difinoj. Estas daŭre farendaĵoj por tio, la
pruvo nun nur validas malgrandparte tiujn kondiĉojn. Por
plibonigi la pruvon, oni uzas difinadon de taktikojn por la pecoj
de kodo kiuj ofte revenas, aŭ oni eĉ uzas mezajn lemojn.

\section{Rilataj verkoj}

\subsection*{CompCert C}

CompCert C estas C kompililo certigita en Coq, komencita kaj
estrata de Xavier Leroy. Ĝia unu versio estis publikigita en 2008
kaj ĝi faras optimumigojn ekvivalentajn al GCC kun la optimumiga
opcio -01.

La artikolo \cite{csmith} traktas interalie na-CompCert kaj oni
vidas ke CompCert estas efektive korekta (certigita programo ne
povas malkongrui siajn specifojn (kondiĉe ke oni fidas na-Coq),
sed tiuj specifoj povas havi erarojn).

CompCert uzas 10 mezajn lingvojn por iri de C al la asembla
lingvo. Tial, oni imagas ke la uzo de la ostaraj semantikoj
povus ebligi multe gajni pri specifoj.

\subsection*{CakeML}

CakeML estas lingvo bazita sur ML kaj ekosistemo konstruata
ĉirkaŭ tiu lingvo. Ĝia semantiko estas difinata kiel grandpaŝa
semantiko specifita en HOL kaj ĝia kompililo estas pruvata. La
kompililo ankaŭ uzas 9 malsamajn lingvojn, kaj denove, la uzo de
la ostara semantiko povus alporti simplecon.

\subsection*{Double WP}

Freŝdate, Martin Clochard kaj Léon Gondelman proponis ilojn
uzanta Why3 por provi pruvi kompililojn en maniero tiom aŭtomata
kiom eble \cite{WP}.
Ili uzas tiun ilon por kompili etan imperativan lingvon (IMP). La
metodo uzita de la aŭtoroj estas ja tre aŭtomata, sed ``implicas
ne malzorgeblan koston en specifoj''. Oni povus imagi kombini la
ideon de la ostarajn semantikojn kun Double WP por gajni en
aŭtomatigo de la specifo.

\subsection*{Ott, Lem kaj \(\mathds{K}\)}

Ott\cite{ott} estas metalingvo kiu estis difinata por priskribi
la operacian semantikon de lingvo. Estas facile legebla kaj
tradukebla sintakso, ĝis \LaTeX{} sed ankaŭ ĝis Coq, Isabelle, aŭ aliaj pruviloj.

Lem\cite{lem} estis enkondukata por provi korekti kelkajn
difektojn de Ott, nome la fakto ke Ott ne ebligas esprimi
proprecojn pri la semantiko kiun ĝi difinas, kaj tio malfortigas
interalie ĝian transporteblecon. Lem ankaŭ aldonas kvantizantojn.

\(\mathds{K}\) estas metalingvo por difini semantikojn. Ĝi uzas
regulojn de reskribado kaj malgrandpaŝan semantikon.

Tiuj iloj do ankaŭ ebligas difini semantikojn sed estas pli
malfacile uzeblaj. Plie, ili ne ebligas difini la interpreton
sendepende je la uzata ilo. La ostara semantiko estas pli malpeza
do pli fleksebla. Ekzemple, estis tre facila reuzi na-necro por
krei na-Coq generilon, kaj oni povus adapti ilin por generi
ekzemple na-Isabelle kodon.

\section{Estontaj verkoj}

\subsection*{malgrandpaŝa semantiko}

La ostara semantiko estas grandpaŝa semantiko. CompCert ankaŭ
funkciis kun grandpaŝa semantiko, sed fine elektis malgrandpaŝan
semantikon ĉar ĝi pli facile ebligis enkalkuli la efikojn. Tiel,
la rezulto de taksado ne estas valoro kiel en la kazo de la
ostara semantiko sed spuro. Tiel, oni povas esprimi la korekteco
rekte per proprecoj pri la spuro, kiu interalie faciligas la
enkalkulon de la interagoj kun la ekstera medio kaj de la
operaciumaj alvokoj.

Tial, oni volus scii ĉu oni povas interpreti la ostaran
semantikon de lingvo kiel malgrandpaŝa semantiko. La fundamenta
malsameco kun la grandpaŝa semantiko estas la neceso rekonstrui
termon post ĉiu kalkula paŝo. En la kazo de la ostara semantiko,
tiu povus krei malfacilaĵojn, ekzemple kiam oni eniras
branĉigon.

\subsection*{Aŭtomatigado}

Ĉi-momente, la generado de Coq kodo estas aŭtomata, sed la pruvoj
daŭre estas tre man-faritaj.

Unuflanke, oni ŝatus havi lingvon por doni la specifojn de la
filtrostoj (la fakto ke \texttt{push} kaj \texttt{pop} estas
inverso ekzemple, aŭ ke \texttt{push} estas injekta), kaj povi
aŭtomate generi la aksiomojn el tio; aliflanke oni devus difini na-Coq taktikojn por faciligi la pruvon kaj igi ĝin pli ``alta nivela''.

\subsection*{Caml light \(\rightarrow\) Zinc}

En \cite{zinc}, Xavier Leroy proponas kompililon de lingvo kiu
naskis na-Caml light. La fonta lingvo de la kompililo estas
interpretata de abstrakta maŝino nomita Zinc (por Zinc Is Not
Caml). La OCaml bajtkodo ĉi-momente uzata estas sufiĉe proksima
de tiu lingvo.

Ĉi-momente, neniu specifo de Caml light ekzistas, nek de OCaml.
Tamen, la pruvata kompililo de CakeML donas formalan semantikon
de ĝin, do ĝi donas formalan semantikon al ML. Same, SML havas
formalan specifon \cite{SML}.

Oni povus doni ostaran semantikon de Caml light kaj kontroli la
korektecon de kompililo el Caml light ĝis Zinc. Tiu plie ebligus
kontroli ĉu la ideo funkcias grandskale.

\bibliographystyle{plain}
\bibliography{rapport}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
